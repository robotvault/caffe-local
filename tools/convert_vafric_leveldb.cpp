// Copyright 2014 BVLC and contributors.
//
// This script converts the CIFAR dataset to the leveldb format used
// by caffe to perform classification.
// Usage:
//    convert_cifar_data input_folder output_db_file
// The CIFAR dataset could be downloaded at
//    http://www.cs.toronto.edu/~kriz/cifar.html

#include <google/protobuf/text_format.h>
#include <glog/logging.h>
#include <leveldb/db.h>

#include <stdint.h>
#include <fstream>  // NOLINT(readability/streams)
#include <string>

#include "caffe/proto/caffe.pb.h"

#include <VaFRIC/VaFRIC.h>

using std::string;


dataset::vaFRIC *vaFRIC_data_reader;

const int kCIFARSize         = 32;
const int kCIFARImageNBytes  = 3072;
const int kCIFARBatchSize    = 10000;
const int kCIFARTrainBatches = 5;



//void read_image(std::ifstream* file, int* label, char* buffer)
//{
//  char label_char;
//  file->read(&label_char, 1);
//  *label = label_char;
//  file->read(buffer, kCIFARImageNBytes);
//  return;
//}

void convert_dataset(const string& input_folder, const string& output_folder)
{
  /// Leveldb options

//  leveldb::Options options;
//  options.create_if_missing = true;
//  options.error_if_exists = true;

//  /// Data buffer

//  int label;

//  char str_buffer[kCIFARImageNBytes];


//  string value;
//  caffe::Datum datum;

//  datum.set_channels(1);
//  datum.set_height(240);
//  datum.set_width(320);

//  LOG(INFO) << "Writing Training data";

//  leveldb::DB* train_db;
//  leveldb::Status status;

//  status = leveldb::DB::Open(options,
//                             output_folder + "/vafric-train-leveldb",
//                             &train_db);

//  CHECK(status.ok()) << "Failed to open leveldb.";

//  //for (int fileid = 0; fileid < kCIFARTrainBatches; ++fileid)
//  {
//    /// Open files
////    LOG(INFO) << "Training Batch " << fileid + 1;
////    snprintf(str_buffer, kCIFARImageNBytes, "/data_batch_%d.bin", fileid + 1);

////    std::ifstream data_file((input_folder + str_buffer).c_str(),
////                            std::ios::in | std::ios::binary);

////    CHECK(data_file) << "Unable to open train file #" << fileid + 1;

//    for (int itemid = 0; itemid < 1000; ++itemid)
//    {
//      //read_image(&data_file, &label, str_buffer);
//      vaFRIC_data_reader->getEuclidean2PlanarDepth(itemid,0,depth_array);

//      datum.add_label(label);
//      datum.set_data(str_buffer, kCIFARImageNBytes);
//      datum.SerializeToString(&value);

//      snprintf(str_buffer,
//               kCIFARImageNBytes, "%05d",
//               fileid * kCIFARBatchSize + itemid);

//      train_db->Put(leveldb::WriteOptions(), string(str_buffer), value);
//    }
//  }

//  delete train_db;
//  delete test_db;
}

int main(int argc, char** argv)
{
  if (argc != 3)
  {
    printf("This script converts the CIFAR dataset to the leveldb format used\n"
           "by caffe to perform classification.\n"
           "Usage:\n"
           "    convert_cifar_data input_folder output_folder\n"
           "Where the input folder should contain the binary batch files.\n"
           "The CIFAR dataset could be downloaded at\n"
           "    http://www.cs.toronto.edu/~kriz/cifar.html\n"
           "You should gunzip them after downloading.\n");
  }
  else
  {
      vaFRIC_data_reader = new dataset::vaFRIC("/home/ankur/workspace/code/AnnotationGT",
                                               320,
                                               240,
                                               319.5/2.0f,
                                               239.5/2.0f,
                                               480.0/2.0f,
                                               -480.0/2.0f);

      google::InitGoogleLogging(argv[0]);
      convert_dataset(string(argv[1]), string(argv[2]));
  }
  return 0;
}

// Copyright 2014 BVLC and contributors.
//
// This is a simple script that allows one to quickly test a network whose
// structure is specified by text format protocol buffers, and whose parameter
// are loaded from a pre-trained network.
// Usage:
//    test_net net_proto pretrained_net_proto iterations [CPU/GPU]

#include <cuda_runtime.h>

#include <cstring>
#include <cstdlib>
#include <vector>

#include <iostream>

#include "caffe/caffe.hpp"

using namespace caffe;  // NOLINT(build/namespaces)

using namespace std;

int main(int argc, char** argv) {
  if (argc < 4 || argc > 6) {
    LOG(ERROR) << "test_net net_proto pretrained_net_proto iterations "
        << "[CPU/GPU] [Device ID]";
    return 1;
  }

  Caffe::set_phase(Caffe::TEST);

  if (argc >= 5 && strcmp(argv[4], "GPU") == 0)
  {
    Caffe::set_mode(Caffe::GPU);
    int device_id = 0;
    
    if (argc == 6)
    {
      device_id = atoi(argv[5]);
    }
    
    Caffe::SetDevice(device_id);
    LOG(ERROR) << "Using GPU #" << device_id;
  } 
  else 
  {
    LOG(ERROR) << "Using CPU";
    Caffe::set_mode(Caffe::CPU);
  }

  Net<double> caffe_test_net(argv[1]);

  std::cout<<"Set up the network" << std::endl;

  caffe_test_net.CopyTrainedLayersFrom(argv[2]);

  int total_iter = atoi(argv[3]);
  LOG(ERROR) << "Running " << total_iter << " iterations.";


  ifstream iFile("test.txt");

  char readlinedata[200];

  int label, red, green, blue;

  std::vector<int>red_vec;
  std::vector<int>green_vec;
  std::vector<int>blue_vec;

  while(1)
  {
      iFile.getline(readlinedata,200);

      istringstream iss(readlinedata);

      iss >> label;
      iss >> red;
      iss >> green;
      iss >> blue;

      if(iFile.eof())
          break;

      red_vec.push_back(red);
      green_vec.push_back(green);
      blue_vec.push_back(blue);
  }


  for(int i = 0 ; i < red_vec.size() ; i++ )
  {
      std::cout<<"i = " << i << " red = " << red_vec[i]<<", green = " << green_vec[i] <<", blue = " << blue_vec[i] << std::endl;
  }

  double test_accuracy = 0;

  double* prob_;


  CVD::Image<CVD::Rgb<CVD::byte> >image2Write(CVD::ImageRef(80,60));

//  double* test_accuracy;
  for (int i = 0; i < 1/*total_iter*/; ++i)
  {
    const vector< Blob<double>* >& result = caffe_test_net.ForwardPrefilled();
    LOG(ERROR) << "Finished the forward prefilled";

    test_accuracy = result[0]->cpu_data()[0];

    std::cout << "result[0].width     = " << result[0]->width() << std::endl;
    std::cout << "result[0].height    = " << result[0]->height() << std::endl;
    std::cout << "result[0].channels  = " << result[0]->channels() << std::endl;
    std::cout << "result[0].num       = " << result[0]->num() << std::endl;

    std::cout <<" test_accracy = "<< test_accuracy << std::endl;

    prob_ = const_cast<double*>(result[1]->cpu_data());

    std::cout << "result[1].width     = " << result[1]->width() << std::endl;
    std::cout << "result[1].height    = " << result[1]->height() << std::endl;
    std::cout << "result[1].channels  = " << result[1]->channels() << std::endl;
    std::cout << "result[1].num       = " << result[1]->num() << std::endl;

//    int idx = i*width*height + width * yy + xx;

//    loss += -log(max(prob_data[xx+width*yy+width*height*static_cast<int>(label[idx]) + i*width*height*dim],
//                 Dtype(FLT_MIN)));


    int num_images = 1/*1000*/, height = 60, width = 80, channels = 23;

    for(int num = 0 ; num < num_images; num++)
    {
        for(int yy = 0; yy < height; yy++)
        {
            for(int xx = 0 ; xx < width; xx++)
            {
                double prob_val = -9999.9;
                int prob_max_id = 0;

                for(int dim = 0; dim < channels; dim++)
                {

                    if ( prob_val < prob_[num*width*height*channels + dim*height*width + yy*width + xx])
                    {
                        prob_val  = prob_[num*width*height*channels + dim*height*width + yy*width + xx];
                        prob_max_id = dim;
                    }

//                    std::cout<<" dim = " << dim <<" vals = " << prob_[num*width*height*channels + dim*height*width + yy*width + xx];
//                    std::cout<<" prob_max_id = " << prob_max_id <<", prob_val = " << prob_val;


                }

//                std::cout<<std::endl;

//                std::cout<<"prob_val = " << prob_val <<", prob_max_id = "<< prob_max_id << std::endl;

                CVD::Rgb<CVD::byte>pixel_colour;

                pixel_colour.red   =   red_vec[prob_max_id];
                pixel_colour.green = green_vec[prob_max_id];
                pixel_colour.blue  =  blue_vec[prob_max_id];

                image2Write[CVD::ImageRef(xx,yy)] = pixel_colour;

            }
        }

        char fileName[200];

        sprintf(fileName,"predicted_result_%02d_%04d.png",i,num);

        CVD::img_save(image2Write,fileName);
    }

    LOG(ERROR) << "Batch " << i << ", accuracy: " << result[0]->cpu_data()[0];
  }
//  test_accuracy /= total_iter;
//  LOG(ERROR) << "Test accuracy: " << test_accuracy;

  return 0;
}

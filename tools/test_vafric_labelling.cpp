#include<iostream>
#include<VaFRIC/VaFRIC.h>


int main(void)
{
    dataset::vaFRIC* vaFRIC_data_reader = new dataset::vaFRIC("/home/ankur/workspace/code/AnnotationGT",
                                                              320,
                                                              240,
                                                              319.5/2.0f,
                                                              239.5/2.0f,
                                                              480.0/2.0f,
                                                             -480.0/2.0f);

    std::vector<float>label_array;
    std::vector<float>depth_array;

    vaFRIC_data_reader->getLabelsforImage(0,0,label_array);

    std::cout<<"Entering here" << std::endl;


    for(int img_no = 0; img_no < vaFRIC_data_reader->getNumberofImageFiles(); img_no++)
    {

        vaFRIC_data_reader->getLabelsforImage(img_no,0,label_array);

        float val = *std::max_element(label_array.begin(), label_array.end());
        std::cout<<"val = " << val << std::endl;

        vaFRIC_data_reader->getEuclidean2PlanarDepth(img_no,0,depth_array);

        val = *std::min_element(depth_array.begin(), depth_array.end());
        std::cout<<"min depth = " << val << std::endl;

        vaFRIC_data_reader->writeLabeltoImage(img_no,0,label_array);

//        getchar();
    }


//    CVD::Image< CVD::Rgb<CVD::byte> >labelImage(CVD::ImageRef(320,240));
//    CVD::img_load(labelImage,"/home/ankur/workspace/code/AnnotationGT/scene_00_0000.png");

//    int height = labelImage.size().y;
//    int width  = labelImage.size().x;

//    std::cout<<"height = " << height <<", width = "<< width << std::endl;

//    int no_labels=0;
//    int colour[255];

//    for(int v = 0 ; v < height; v++)
//    {
//        for(int u = 0 ; u < width; u++)
//        {
//            CVD::Rgb<CVD::byte>rgb_pixel = labelImage[CVD::ImageRef(u,v)];

//            int label = rgb_pixel.red + rgb_pixel.green*255 + rgb_pixel.blue*255*255;

//            int label_exists = false;

//            for(int i = 0 ; i < no_labels; i++)
//            {
//                if ( colour[i] == label )
//                {
//                    label_exists = true;
//                    break;
//                }
//            }

//            if (!no_labels || !label_exists)
//            {
//                colour[no_labels++] = label;
//                std::cout<<"label = " << label << std::endl;
//            }

//            label_array.push_back(no_labels);
//        }
//    }

//     std::cout<<"Number of distinct labels = " << no_labels<< std::endl;

//     sort(label_array.begin(), label_array.end());

//     float val = *std::max_element(label_array.begin(), label_array.end());

//     std::cout<<"val = " << val << std::endl;

//    /// using default comparison:
//     std::vector<float>::iterator unique_end = std::unique (label_array.begin(), label_array.end());

//     int ndistinct = distance(label_array.begin(),unique_end);

//     std::cout<<"ndistinct = " << ndistinct << std::endl;


}

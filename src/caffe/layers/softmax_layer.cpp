// Copyright 2014 BVLC and contributors.
//
#include <algorithm>
#include <vector>

#include "caffe/layer.hpp"
#include "caffe/vision_layers.hpp"
#include "caffe/util/math_functions.hpp"

#include <iostream>

using std::max;

namespace caffe {

template <typename Dtype>
void SoftmaxLayer<Dtype>::SetUp(const vector<Blob<Dtype>*>& bottom,
                                      vector<Blob<Dtype>*>* top)
{

    Layer<Dtype>::SetUp(bottom, top);

    (*top)[0]->Reshape(bottom[0]->num(), bottom[0]->channels(),
            bottom[0]->height(), bottom[0]->width());

    sum_multiplier_.Reshape(1, bottom[0]->channels(),
            1, 1);

    Dtype* multiplier_data = sum_multiplier_.mutable_cpu_data();

    for (int i = 0; i < sum_multiplier_.count(); ++i)
    {
        multiplier_data[i] = 1.;
    }

    //  scale_.Reshape(bottom[0]->num(), 1, 1, 1);

    ///New
    scale_.Reshape(bottom[0]->num(), 1, bottom[0]->height(), bottom[0]->width());

}


template <typename Dtype>
Dtype SoftmaxLayer<Dtype>::Forward_cpu_dense(const vector<Blob<Dtype>*>& bottom,
    vector<Blob<Dtype>*>* top)
{

//  std::cout <<"Entering the forward pass of the softmax layer" << std::endl;

  const Dtype* bottom_data = bottom[0]->cpu_data();
  Dtype* top_data          = (*top)[0]->mutable_cpu_data();
  Dtype* scale_data        = scale_.mutable_cpu_data();


  int num = bottom[0]->num();
  int dim = bottom[0]->channels();

  int width  = bottom[0]->width();
  int height = bottom[0]->height();

  ///num = 100
  ///dim = 10
  ///count = 1000

  caffe_copy(bottom[0]->count(), bottom_data, top_data);

  /// we need to subtract the max to avoid numerical issues, compute the exp,
  /// and then normalize.

  /// For each image in the batch compute the maximum value
  /// and then subtract that value from the data

//  LOG(INFO)<<"Going to find the max of the data";

//  LOG(INFO)<<"\n scale: height     = " << scale_.height()
//           <<"\n scale: width      = " << scale_.width()
//           <<"\n scale: num        = " << scale_.num()
//           <<"\n scale: channels   = " << scale_.channels()
//           <<"\n scale: count      = " << scale_.count();

//  LOG(INFO)<<"\n b.height   = " << bottom[0]->height()
//           <<"\n b.width    = " << bottom[0]->width()
//           <<"\n b.num      = " << bottom[0]->num()
//           <<"\n b.channels = " << bottom[0]->channels()
//           <<"\n count      = " << bottom[0]->count();
//  LOG(INFO)<<"\n dim        = " << dim;

//  LOG(INFO)<<"\n t.height   = " << (*top)[0]->height()
//           <<"\n t.width    = " << (*top)[0]->width()
//           <<"\n t.num      = " << (*top)[0]->num()
//           <<"\n t.channels = " << (*top)[0]->channels()
//           <<"\n t.count    = " << (*top)[0]->count();


//  for( int n_imgs = 0; n_imgs < num; n_imgs++ )
//  {
//      std::cout<<"========================= Image No: " << n_imgs << std::endl;

//      for(int xx = 0 ; xx < width ; xx++ )
//      {
//          for(int yy = 0; yy < height ; yy++)
//          {
//              std::cout<<"[ "<<xx<< ","<<yy<<" ] = ";
//              for( int c_imgs = 0; c_imgs < dim; c_imgs++ )
//              {
//                   std::cout<<" "<<bottom_data[n_imgs*width*height*dim + c_imgs*height*width + yy*height + xx];
//              }
//              std::cout<<std::endl;
//          }
//      }

//  }


//#pragma omp parallel for collapse(3)

  /// Figure out the max element from each bottom_data image of d dimensions.
  for (int i = 0; i < num; i++)
  {
      for(int xx = 0 ; xx < width ; xx++ )
      {
          for(int yy = 0; yy < height ; yy++)
          {
                scale_data[i*width*height + xx + yy*width] = bottom_data[i*width*height*dim + yy*height + xx];

                for(int j = 0 ; j < dim ; j++)
                {
                    scale_data[i*width*height + xx + yy*width] = max(scale_data[i*width*height + xx + yy*width],
                                                                     bottom_data[i*width*height*dim + j*height*width + yy*height + xx]);
                }

                if ( isnan(scale_data[i*width*height + xx + yy*width])
                     || isinf(scale_data[i*width*height + xx + yy*width]) )
                {
                    LOG(INFO) << "There is something wrong in here with scale_data .. " << scale_data[i*width*height + xx + yy*width];
                    getchar();
                }
          }
      }
  }


//  for( int n_imgs = 0; n_imgs < num; n_imgs++ )
//  {
////      std::cout<<"n_imgs = " << n_imgs << std::endl;
//      for(int xx = 0 ; xx < width ; xx++ )
//      {
//          for(int yy = 0; yy < height ; yy++)
//          {
//              if( !scale_data[n_imgs*width*height + xx + yy*width] )
//              std::cout <<" xx = "<<xx<<", yy = " << yy << scale_data[n_imgs*width*height + xx + yy*width];
//          }
//      }
////      std::cout << std::endl;
//  }


//  LOG(INFO) <<"Obtaining the scale_data, max per pixel";

  /// https://software.intel.com/sites/products/documentation/hpc/mkl/mklman/GUID-97718E5C-6E0A-44F0-B2B1-A551F0F164B2.htm
  /// subtraction
  /// C := alpha*op(A)*op(B) + beta*C,
  /// where:
  /// op(x) is one of op(x) = x, or op(x) = x', or op(x) = conjg(x'),
  /// alpha and beta are scalars,
  /// A, B and C are matrices:
  /// op(A) is an m-by-k matrix,
  /// op(B) is a k-by-n matrix,
  /// C is an m-by-n matrix.

//  void caffe_cpu_gemm(const CBLAS_TRANSPOSE TransA,
//                      const CBLAS_TRANSPOSE TransB,
//                      const int M,
//                      const int N,
//                      const int K,
//                      const Dtype alpha,
//                      const Dtype* A, const Dtype* B, const Dtype beta,
//                      Dtype* C);

//  caffe_cpu_gemm<Dtype>(CblasNoTrans,
//                        CblasNoTrans,
//                        num * width * height,
//                        dim,
//                        1 ,
//                        -1., /// alpha
//                        scale_data,
//                        sum_multiplier_.cpu_data(),
//                        1.,  /// beta
//                        top_data);


  /// Subtract the maximum from each element in the channel

//#pragma omp parallel for collapse(2)

//  for(int i = 0 ; i < num * dim * width * height ; i++ )
//  {
//      if ( isnan(top_data[i]) || isinf(top_data[i]) )
//      {
//          LOG(INFO)<<" something wrong in top before subtracting the max term: "<< top_data[i];
//          getchar();
//      }
//  }

//#pragma omp parallel for collapse(1)

  for(int batch_img = 0; batch_img < num; batch_img++ )
  {
      for(int d = 0; d < dim; d++ )
      {
          caffe_cpu_axpby<Dtype>(width*height,
                                 (Dtype)-1.0,
                                 scale_data + batch_img * width * height,
                                 (Dtype)(1.0),
                                 top_data
                                 + batch_img * width * height * dim
                                 + d * width * height);
      }

  }

//  for( int n_imgs = 0; n_imgs < num; n_imgs++ )
//  {
//      std::cout<<"After Subtraction, Image No: " << n_imgs << std::endl;

//      for(int xx = 0 ; xx < width ; xx++ )
//      {
//          for(int yy = 0; yy < height ; yy++)
//          {
//              std::cout<<"[ "<<xx<< ","<<yy<<" ] = ";
//              for( int c_imgs = 0; c_imgs < dim; c_imgs++ )
//              {
//                   std::cout<<" "<<top_data[n_imgs*width*height*dim + c_imgs*height*width + yy*height + xx];
//              }
//              std::cout<<std::endl;
//          }
//      }

//  }






//  LOG(INFO) <<"Subtracted the max per pixel";

  /// Perform exponentiation: It cannot go wrong ..
//  caffe_exp<Dtype>(num * dim * width * height, top_data, top_data);

//#pragma omp parallel for simd
#pragma omp parallel for
   for(int i = 0; i < num * dim * width * height ; i++)
       top_data[i] = exp(top_data[i]);


//  for( int n_imgs = 0; n_imgs < num; n_imgs++ )
//  {
//      std::cout<<"Exponentiated the values, Image No: " << n_imgs << std::endl;

//      for(int xx = 0 ; xx < width ; xx++ )
//      {
//          for(int yy = 0; yy < height ; yy++)
//          {
//              std::cout<<"[ "<<xx<< ","<<yy<<" ] = ";
//              for( int c_imgs = 0; c_imgs < dim; c_imgs++ )
//              {
//                   std::cout<<" "<<top_data[n_imgs*width*height*dim + c_imgs*height*width + yy*height + xx];
//              }
//              std::cout<<std::endl;
//          }
//      }

//  }


//  LOG(INFO) <<"Exponentiated the values";


//  bool fishy = false;

//  for(int i = 0 ; i < num * dim * width * height ; i++ )
//  {
//      if ( isnan(top_data[i]) || isinf(top_data[i]) )
//      {
//          LOG(INFO)<<" something wrong in top after exponentiation: "<< top_data[i];
//          fishy = true;
//      }
//  }

//  if ( fishy )
//  {
//      for(int i = 0 ; i < num * dim * width * height ; i++ )
//      {
//          std::cout<<" "<< top_data[i];
//      }
//  }


  /// sum after exp
//#pragma omp parallel for collapse(1)
  for(int batch_img = 0; batch_img < num; batch_img++)
  {

      Dtype *sum_exp = new Dtype[width*height];

      for(int i=0; i < width*height; i++)
          sum_exp[i] = (Dtype)(0.0);

      for(int d = 0; d < dim; d++ )
      {

          caffe_add<Dtype>(width*height,
                           top_data + d * width * height + batch_img * width * height * dim,
                           sum_exp,
                           sum_exp);
      }

      caffe_copy<Dtype>(width*height,
                        sum_exp,
                        scale_data + batch_img * width* height);

      for(int i=0;i<width*height;i++)
      {
          if ( !scale_data[batch_img*width*height+i] )
              std::cerr<<"scale_data is zero" << std::endl;
      }

      delete sum_exp;

  }

//  std::cout<<"Summed the exponentiated values.." << std::endl;

//  for( int n_imgs = 0; n_imgs < num; n_imgs++ )
//  {
//      std::cout<<"n_imgs = " << n_imgs << std::endl;
//      for(int xx = 0 ; xx < width ; xx++ )
//      {
//          for(int yy = 0; yy < height ; yy++)
//          {
//              std::cout <<" "<<scale_data[n_imgs*width*height + xx + yy*width];
//          }
//      }
//      std::cout << std::endl;

//  }

//  #pragma omp parallel for collapse(4)
   for(int batch_img = 0; batch_img < num; batch_img++ )
   {
       for(int d = 0; d < dim ; d++ )
       {
           caffe_div<Dtype>(width*height,
                           top_data + d * width * height + batch_img * width * height * dim,
                           scale_data + width * height * batch_img,
                           top_data + d * width * height + batch_img * width * height * dim);
//           for(int y = 0; y < height ; y++ )
//           {
//               for(int x = 0; x< width; x++)
//               {

//                   top_data[y*width+x + d*width*height + batch_img * width * height * dim] /= scale_data[batch_img*width*height + y*width+x];

//                   if (isnan(top_data[y*width+x + d*width*height + batch_img * width * height * dim]))
//                   {
//                       std::cout<<"scale_data = " << scale_data[batch_img*width*height + y*width+x] << std::endl;
//                       getchar();
//                   }
//               }
//           }
       }
   }


//  LOG(INFO)<<"Division with the scale factor";

  return Dtype(0);

}




template <typename Dtype>
Dtype SoftmaxLayer<Dtype>::Forward_cpu(const vector<Blob<Dtype>*>& bottom,
    vector<Blob<Dtype>*>* top)
{


  return Forward_cpu_dense(bottom,top);

  const Dtype* bottom_data = bottom[0]->cpu_data();
  Dtype* top_data          = (*top)[0]->mutable_cpu_data();
  Dtype* scale_data        = scale_.mutable_cpu_data();


  int num = bottom[0]->num();
  int dim = bottom[0]->count() / bottom[0]->num();

  int width = bottom[0]->width();
  int height = bottom[0]->height();


  ///num = 100
  ///dim = 10
  ///count = 1000

//  int width_top  = (*top)[0]->width();
//  int height_top = (*top)[0]->height();

//  std::cout<<"width = " << width <<", height = " << height << std::endl;
//  std::cout<<"width_top = " << width_top <<", height = " << height_top << std::endl;

  caffe_copy(bottom[0]->count(), bottom_data, top_data);

  /// we need to subtract the max to avoid numerical issues, compute the exp,
  /// and then normalize.

  /// For each image in the batch compute the maximum value
  /// and then subtract that value from the data

  for (int i = 0; i < num; ++i)
  {
    scale_data[i] = bottom_data[i*dim];

    ///New I think we need to make changes here
    ///dim should be replaced with width*height*dim
    for (int j = 0; j < dim; ++j)
    {
      scale_data[i] = max(scale_data[i], bottom_data[i * dim + j]);
    }    
  }

  /// subtraction

  caffe_cpu_gemm<Dtype>(CblasNoTrans, CblasNoTrans, num, dim, 1, -1.,
    scale_data, sum_multiplier_.cpu_data(), 1., top_data);

  /// Perform exponentiation
  caffe_exp<Dtype>(num * dim, top_data, top_data);

  /// sum after exp
  caffe_cpu_gemv<Dtype>(CblasNoTrans, num, dim, 1., top_data,
      sum_multiplier_.cpu_data(), 0., scale_data);

  /// Do division
  for (int i = 0; i < num; ++i)
  {
    caffe_scal<Dtype>(dim, Dtype(1.) / scale_data[i], top_data + i * dim);
  }

  return Dtype(0);
}

/// http://stats.stackexchange.com/questions/79454/softmax-layer-in-a-neural-network
template <typename Dtype>
void SoftmaxLayer<Dtype>::Backward_cpu_dense(
        const vector<Blob<Dtype>*>& top,
        const vector<bool>& propagate_down,
        vector<Blob<Dtype>*>* bottom)
{
  const Dtype* top_diff = top[0]->cpu_diff();
  const Dtype* top_data = top[0]->cpu_data();

  Dtype* bottom_diff = (*bottom)[0]->mutable_cpu_diff();
  Dtype* scale_data = scale_.mutable_cpu_data();

  int num    = top[0]->num();
  int dim    = top[0]->channels(); //top[0]->count() / top[0]->num();
  int width  = top[0]->width();
  int height = top[0]->height();

//  LOG(INFO)<<"\n xxxxxxxxxxxxxx t.height   = " << height
//           <<"\n xxxxxxxxxxxxxx t.width    = " << width
//           <<"\n xxxxxxxxxxxxxx t.num      = " << num
//           <<"\n xxxxxxxxxxxxxx t.channels = " << top[0]->channels()
//           <<"\n xxxxxxxxxxxxxx t.count    = " << top[0]->count()
//           <<"\n xxxxxxxxxxxxxx t.dmcounted = " << top[0]->count() / top[0]->num();


//  LOG(INFO)<<"Bakward propagation in Softmax Layer: Hit Enter";
//  getchar();


//  std::cout << "top_diff = " << std::endl;
//  for(int i = 0; i < top[0]->count(); i++)
//      std::cout<<" "<<top_diff[i];

//  std::cout<<std::endl;

  caffe_copy(top[0]->count(), top_diff, bottom_diff);

  /// Compute inner1d(top_diff, top_data) and subtract them from the bottom diff
//  for (int i = 0; i < num; ++i)
//  {
//      scale_data[i] = caffe_cpu_dot<Dtype>(dim*height*width,
//                                           top_diff + i * dim * width * height,
//                                           top_data + i * dim * width * height);
//  }

//  std::cout<<"scale_data before" << std::endl;

//  for(int batch_img = 0; batch_img < num; batch_img++ )
//  {
//      for(int yy = 0; yy < height ; yy++)
//      {
//          for(int xx = 0; xx < width; xx++ )
//          {
//              std::cout<<" " <<scale_data[yy*width+xx + width*height*batch_img];
//          }
//      }

//      std::cout<<std::endl;
//  }

  for(int i = 0; i < num*width*height; i++)
      scale_data[i] = (Dtype)(0.0);

  for(int batch_img = 0; batch_img < num; batch_img++ )
  {
      Dtype* mul_top_diff_top_data = new Dtype[width*height];

      for(int i = 0 ; i < width*height; i++)
          mul_top_diff_top_data[i] = (Dtype)(0.0);

      for(int d = 0 ;d  < dim; d++ )
      {
        caffe_mul<Dtype>(width*height,
                         top_diff + batch_img * width* height * dim
                         + width * height * d,
                         top_data + batch_img * width* height * dim
                         + width * height * d,
                         mul_top_diff_top_data);

        caffe_add<Dtype>(width*height,
                         mul_top_diff_top_data,
                         scale_data + batch_img * width * height,
                         scale_data + batch_img * width * height);
      }
  }

  std::cout<<"scale_data after" << std::endl;

//  for(int batch_img = 0; batch_img < num; batch_img++ )
//  {
//      for(int yy = 0; yy < height ; yy++)
//      {
//          for(int xx = 0; xx < width; xx++ )
//          {
//              std::cout<<" " <<scale_data[yy*width+xx + width*height*batch_img];
//          }
//      }

//      std::cout<<std::endl;
//  }


  for(int batch_img = 0; batch_img < num; batch_img++)
  {
      for(int d = 0; d < dim; d++)
      {
          caffe_cpu_axpby<Dtype>(width*height,
                                 (Dtype)(-1.0),
                                 scale_data + batch_img * width * height,
                                 (Dtype)(1.0),
                                 bottom_diff + batch_img * width * height * dim
                                 + d * width * height);
      }
  }

  /// subtraction
//  caffe_cpu_gemm<Dtype>(CblasNoTrans,
//                        CblasNoTrans,
//                        num*width*height, dim, 1,
//                        -1., scale_data,
//                        sum_multiplier_.cpu_data(),
//                        1., bottom_diff);

  /// elementwise multiplication
  caffe_mul<Dtype>(top[0]->count(), bottom_diff, top_data, bottom_diff);

//  for(int i = )
}


template <typename Dtype>
void SoftmaxLayer<Dtype>::Backward_cpu(const vector<Blob<Dtype>*>& top,
    const vector<bool>& propagate_down,
    vector<Blob<Dtype>*>* bottom)
{

    Backward_cpu_dense(top,propagate_down,bottom);
    return;



  const Dtype* top_diff = top[0]->cpu_diff();
  const Dtype* top_data = top[0]->cpu_data();

  Dtype* bottom_diff = (*bottom)[0]->mutable_cpu_diff();
  Dtype* scale_data = scale_.mutable_cpu_data();

  int num = top[0]->num();
  int dim = top[0]->count() / top[0]->num();

  caffe_copy(top[0]->count(), top_diff, bottom_diff);

  // Compute inner1d(top_diff, top_data) and subtract them from the bottom diff
  for (int i = 0; i < num; ++i) {
    scale_data[i] = caffe_cpu_dot<Dtype>(dim, top_diff + i * dim,
        top_data + i * dim);
  }

  // subtraction
  caffe_cpu_gemm<Dtype>(CblasNoTrans, CblasNoTrans, num, dim, 1, -1.,
      scale_data, sum_multiplier_.cpu_data(), 1., bottom_diff);

  // elementwise multiplication
  caffe_mul<Dtype>(top[0]->count(), bottom_diff, top_data, bottom_diff);
}


INSTANTIATE_CLASS(SoftmaxLayer);


}  // namespace caffe

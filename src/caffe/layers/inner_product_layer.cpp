// Copyright 2014 BVLC and contributors.

#include <vector>

#include "caffe/blob.hpp"
#include "caffe/common.hpp"
#include "caffe/filler.hpp"
#include "caffe/layer.hpp"
#include "caffe/vision_layers.hpp"
#include "caffe/util/math_functions.hpp"

namespace caffe {


/// https://github.com/sguada/caffe-public/blob/L2_hinge_loss/src/caffe/layers/inner_product_layer.cpp/

template <typename Dtype>
void InnerProductLayer<Dtype>::SetUp(const vector<Blob<Dtype>*>& bottom,
                                           vector<Blob<Dtype>*>* top)
{

  Layer<Dtype>::SetUp(bottom, top);

  const int num_output = this->layer_param_.inner_product_param().num_output();  
  bias_term_ = this->layer_param_.inner_product_param().bias_term();

  /// Figure out the dimensions
  M_ = bottom[0]->num();                      /// number of images in a batch
  K_ = bottom[0]->channels();//bottom[0]->count() / bottom[0]->num(); /// number of input channels I think!
  N_ = num_output;                            /// number of output channels


  LOG(INFO) <<"M_ = "<< M_ <<", K_ = " << K_ <<", N_ = " << N_ << std::endl;
//  getchar();

  (*top)[0]->Reshape(bottom[0]->num(), num_output, bottom[0]->height(), bottom[0]->width());

  /// Check if we need to set up the weights
  if (this->blobs_.size() > 0)
  {
    LOG(INFO) << "Skipping parameter initialization";
  }

  else
  {
    LOG(INFO) << "Entering into else loop";

    if (bias_term_)
    {
      this->blobs_.resize(2);
    }
    else
    {
      this->blobs_.resize(1);
    }

    /// Need to check this...


    /// Intialize the weight
    this->blobs_[0].reset(new Blob<Dtype>(1, 1, N_, K_));

    /// fill the weights
    shared_ptr<Filler<Dtype> > weight_filler(GetFiller<Dtype>(
        this->layer_param_.inner_product_param().weight_filler()));

    weight_filler->Fill(this->blobs_[0].get());


    /// If necessary, initialize and fill the bias term
    if (bias_term_)
    {
//      this->blobs_[1].reset(new Blob<Dtype>(1, 1, 1, N_));
//      shared_ptr<Filler<Dtype> > bias_filler(GetFiller<Dtype>(
//          this->layer_param_.inner_product_param().bias_filler()));
//      bias_filler->Fill(this->blobs_[1].get());

      /// For Image based..
      this->blobs_[1].reset(new Blob<Dtype>(1, N_, bottom[0]->height(), bottom[0]->width()));

//        this->blobs_[1].reset(new Blob<Dtype>(1, 1, 1, N_));

        shared_ptr<Filler<Dtype> > bias_filler(GetFiller<Dtype>(
                                                   this->layer_param_.inner_product_param().bias_filler()));
        bias_filler->Fill(this->blobs_[1].get());
    }


//    Dtype* data = this->blobs_[0]->mutable_cpu_data();

//    int __height = N_;
//    int __width  = K_;

////    std::cout<<"InputData " << std::endl;
////    for(int yy =0 ; yy < __height ; yy++)
////    {
////        for(int xx = 0; xx < __width ; xx++ )
////              std::cout<<" "<<data[yy*__width+xx];

////        std::cout<<std::endl;
////    }
//    getchar();


  }
  /// parameter initialization

  /// Setting up the bias multiplier
  if (bias_term_)
  {
      std::cout<<"Bias term multiplier" << std::endl;
    bias_multiplier_.reset(new SyncedMemory(M_ * sizeof(Dtype)));
    Dtype* bias_multiplier_data =
        reinterpret_cast<Dtype*>(bias_multiplier_->mutable_cpu_data());
    for (int i = 0; i < M_; ++i)
    {
        bias_multiplier_data[i] = 1.;
    }
  }

 ///  std::cout<<"Exiting the Set up loop" << std::endl;

}


template <typename Dtype>
Dtype InnerProductLayer<Dtype>::Forward_cpu_dense(const vector<Blob<Dtype>*>& bottom,
                                                  vector<Blob<Dtype>*>* top)
{
  const Dtype* bottom_data = bottom[0]->cpu_data();
  Dtype* top_data = (*top)[0]->mutable_cpu_data();

  int width    = bottom[0]->width();
  int height   = bottom[0]->height();
  int channels = bottom[0]->channels();

//  LOG(INFO) <<"b. width = " << width              <<", b. height = " << height              <<", b.channels = " << channels;
//  LOG(INFO) <<"t. width = " << (*top)[0]->width() << " t. height = " << (*top)[0]->height() <<", t.channels = " << (*top)[0]->channels();

//  Dtype* weight_times_bottom_data = new Dtype[width*height];

//  for(int i = 0; i < width * height ; i++)
//      weight_times_bottom_data[i] = (Dtype)(0.0);

//  Dtype* sum_weight               = new float[width*height];


  const Dtype* weight = this->blobs_[0]->cpu_data();

//  LOG(INFO) <<" weight width = " << this->blobs_[0]->width() <<", weight.height = " << this->blobs_[0]->height();

//  const Dtype* top_data_block;

//  for(int bimages = 0 ;  bimages < M_ ; bimages++ )
//  {
//      for(int outchannels = 0; outchannels < N_ ; outchannels++)
//      {
//          for(int inchannels = 0; inchannels < K_ ; inchannels++ )
//          {
//              caffe_mul<Dtype>(width*height,
//                               weight      + width*height*inchannels + outchannels*width*height*K_,
//                               bottom_data + inchannels*width*height + bimages*K_*width*height,
//                               weight_times_bottom_data);

//              caffe_add<Dtype>(width*height,
//                               top_data    + width * height * outchannels + bimages * width*height*N_,
//                               weight_times_bottom_data,
//                               top_data    + width * height * outchannels + bimages * width*height*N_);
//          }
//      }
//  }

  const Dtype* bottom_bias = this->blobs_[1]->cpu_data();

  int Outchannels = N_;
  int Inchannels  = K_;

  for(int b_images = 0; b_images < M_ ; b_images++)
  {
      for(int yy = 0; yy < height ; yy++ )
      {
          for(int xx = 0; xx < width ; xx++ )
          {
              for(int dim_out = 0; dim_out < Outchannels; dim_out++ )
              {
                  Dtype w_b_row = (Dtype)(0.0);

                  for(int dim_in = 0; dim_in < Inchannels; dim_in++ )
                  {
                      int idx_in = b_images * width * height * Inchannels
                                    + width * height * dim_in + yy * width + xx;

                      w_b_row += bottom_data[idx_in] * weight[dim_in + dim_out * Inchannels];

                  }

                  int idx_out = b_images * width * height * Outchannels
                                + width * height * dim_out + yy * width + xx;

                  top_data[idx_out] = w_b_row;// + bottom_bias[dim_out];
              }

          }
      }
  }


//  LOG(INFO) <<"Getting out of the loop:1 ";

//  caffe_cpu_gemm<Dtype>(CblasNoTrans, CblasTrans, M_, N_, K_, (Dtype)1.,
//                        bottom_data, weight, (Dtype)0., top_data);

  /// Check if they are all OK ---!

  if (bias_term_)
  {
      const Dtype* bottom_bias = this->blobs_[1]->cpu_data();

      for(int batch_img = 0; batch_img < M_ ; batch_img++)
      {
          caffe_add<Dtype>(width*height*Outchannels,
                           bottom_bias,
                           top_data + batch_img*width*height*Outchannels,
                           top_data + batch_img*width*height*Outchannels);
      }
  }

//  delete weight_times_bottom_data;

  return Dtype(0);
}



template <typename Dtype>
Dtype InnerProductLayer<Dtype>::Forward_cpu(const vector<Blob<Dtype>*>& bottom,
                                                  vector<Blob<Dtype>*>* top)
{

  return Forward_cpu_dense(bottom,top);

  const Dtype* bottom_data = bottom[0]->cpu_data();
  Dtype* top_data = (*top)[0]->mutable_cpu_data();

  const Dtype* weight = this->blobs_[0]->cpu_data();

  caffe_cpu_gemm<Dtype>(CblasNoTrans, CblasTrans, M_, N_, K_, (Dtype)1.,
                        bottom_data, weight, (Dtype)0., top_data);

  if (bias_term_)
  {
    caffe_cpu_gemm<Dtype>(CblasNoTrans, CblasNoTrans, M_, N_, 1, (Dtype)1.,
        reinterpret_cast<const Dtype*>(bias_multiplier_->cpu_data()),
        this->blobs_[1]->cpu_data(), (Dtype)1., top_data);
  }

  return Dtype(0);
}


template <typename Dtype>
void InnerProductLayer<Dtype>::Backward_cpu_dense(const vector<Blob<Dtype>*>& top,
                                                  const vector<bool>& propagate_down,
                                                        vector<Blob<Dtype>*>* bottom)
{

  const Dtype* top_diff    = top[0]->cpu_diff();
  const Dtype* bottom_data = (*bottom)[0]->cpu_data();


  int width  = (*bottom)[0]->width();
  int height = (*bottom)[0]->height();

  Dtype* blob_1_cpu_data = this->blobs_[1]->mutable_cpu_diff(); /// bias
  Dtype* weight_data     = this->blobs_[0]->mutable_cpu_diff(); /// weight


  int t_width  = top[0]->width();
  int t_height = top[0]->height();
  int t_channels = top[0]->channels();


  int b_width    = this->blobs_[1]->width();
  int b_height   = this->blobs_[1]->height();
  int b_channels = this->blobs_[1]->channels();



//  LOG(INFO)<<"t_width = " << t_width <<", t_height = "<< t_height <<", t_channels = "<< t_channels;
//  LOG(INFO)<<"bias_width = " << b_width <<", bias_height = "<< b_height <<", bias_channels = "<< b_channels;


  /// Gradient with respect to weight
  //  caffe_cpu_gemm<Dtype>(CblasTrans,
  //                        CblasNoTrans,
  //                        N_, K_, M_,
  //                        (Dtype)1.,
  //                        top_diff,
  //                        bottom_data,
  //                        (Dtype)0.,
  //                        this->blobs_[0]->mutable_cpu_diff());

  /// TODO: double check if there is any scale factor we need to divide
  /// the data by.

  Dtype* mul_top_diff = new Dtype[width*height];

  int Outchannels = N_;
  int Inchannels  = K_;

  for(int dim_out = 0; dim_out < Outchannels; dim_out++ )
  {
      for(int dim_in = 0; dim_in < Inchannels ; dim_in++ )
      {
          Dtype sum_mul_top_diff = 0;

          for(int batch_img = 0; batch_img < M_; batch_img++ )
          {
              for(int i = 0; i < width*height; i++)
                  mul_top_diff[i] = (Dtype)(0.0);

              caffe_mul<Dtype>(width*height,
                               top_diff    + width*height*Outchannels*batch_img
                               + width*height*dim_out,
                               bottom_data + width*height*Inchannels *batch_img
                               + width*height*dim_in,
                               mul_top_diff);

//              caffe_add<Dtype>(width*height,
//                               mul_top_diff,
//                               weight_data + width*height*dim_in
//                               + width*height*Inchannels*dim_out,
//                               weight_data + width*height*dim_in
//                               + width*height*Inchannels*dim_out);

              for(int yy = 0; yy < height ; yy++ )
              {
                  for(int xx = 0; xx < width; xx++ )
                  {
                      sum_mul_top_diff += mul_top_diff[yy*width+xx];
                  }
              }

//              sum_mul_top_diff += std::accumulate(mul_top_diff,mul_top_diff+width*height,0);

//              if ( sum_mul_top_diff )
//              std::cout<<"sum_mul_top_diff = " << sum_mul_top_diff<<std::endl;

          }

          weight_data[dim_out * Inchannels + dim_in ] = sum_mul_top_diff;
      }
  }

  delete mul_top_diff;

  LOG(INFO) <<"Arriving here at the end of sum_mul_top_diff ";

//  std::cout<<"weight_data " << std::endl;
//  for(int dim_out = 0; dim_out < Outchannels; dim_out++ )
//  {
//      for(int dim_in = 0; dim_in < Inchannels ; dim_in++ )
//      {
//          std::cout<<" " << weight_data[dim_out * Inchannels + dim_in ];
//      }
//      std::cout<<std::endl;
//  }

//  std::cout<<"top diff data" << std::endl;
//  for(int batch_img = 0; batch_img < M_; batch_img++ )
//  {
//      for(int yy = 0; yy < height ; yy++ )
//      {
//          for(int xx = 0; xx < width; xx++ )
//          {
//              for(int dim_out = 0; dim_out < Outchannels; dim_out++ )
//              {
//                  int idx = batch_img * width * height * Outchannels + width*height*dim_out + yy * width + xx;

//                  std::cout<<" " << top_diff[idx];
//              }

//              std::cout<<std::endl;
//          }
//      }
//  }

//  std::cout<<"bottom data" << std::endl;
//  for(int batch_img = 0; batch_img < M_; batch_img++ )
//  {
//      for(int yy = 0; yy < height ; yy++ )
//      {
//          for(int xx = 0; xx < width; xx++ )
//          {
//              for(int dim_in = 0; dim_in < Inchannels; dim_in++ )
//              {
//                  int idx = batch_img * width * height * Inchannels + width*height*dim_in + yy * width + xx;

//                  std::cout<<" " << bottom_data[idx];
//              }

//              std::cout<<std::endl;
//          }
//      }
//  }

//  for(int batch_img = 0; batch_img < M_; batch_img++ )



  if (bias_term_)
  {
    ///  Gradient with respect to bias
    ///  NAME
    ///  DGEMV - perform one of the matrix-vector operations   y :=
    ///  alpha*A*x + beta*y, or y := alpha*A'*x + beta*y,

    ///      SUBROUTINE DGEMV ( TRANS, M, N, ALPHA, A, LDA, X, INCX,
    ///                       BETA, Y, INCY )

      /// TODO: double check if the averaging is done or not
      /// i.e. Do we need scaling here by 1/batch_size?
      /// Need to change it to transpose?

//      LOG(INFO)<<"Coming to the start of bias gradients";

      Dtype* sum_top_diff = new Dtype[width*height];

      for(int dim_out = 0; dim_out < Outchannels; dim_out++)
      {
          for(int i = 0; i < width * height ; i++)
              sum_top_diff[i] = (Dtype)(0.0);

          for(int batch_img = 0 ; batch_img < M_; batch_img++ )
          {
              caffe_add<Dtype>(width*height,
                               top_diff + width*height*dim_out
                               + Outchannels*width*height*batch_img,
                               sum_top_diff,
                               sum_top_diff);
          }

          caffe_copy<Dtype>(width*height,
                            sum_top_diff,
                            blob_1_cpu_data + dim_out*width*height);
          //blob_1_cpu_data[dim_out] = std::accumulate(sum_top_diff,sum_top_diff+width*height,0);

//          Dtype sum_total = (Dtype)(0.0);
//          for(int i = 0; i < width*height; i++)
//              sum_total += sum_top_diff[i];

//          blob_1_cpu_data[dim_out] = sum_total;

      }

      delete sum_top_diff;


//      LOG(INFO)<<"Coming to the end of bias gradients";
  }

  if (propagate_down[0])
  {

      std::cout<<"Is going to propagage down" << std::endl;
//      getchar();

    /// Gradient with respect to bottom data
//    caffe_cpu_gemm<Dtype>(CblasNoTrans,
//                          CblasNoTrans,
//                          M_, K_, N_,
//                          (Dtype)1.,
//                          top_diff,
//                          this->blobs_[0]->cpu_data(),
//                          (Dtype)0.,
//                          (*bottom)[0]->mutable_cpu_diff());


      Dtype* bottom_data_mutable = (*bottom)[0]->mutable_cpu_diff();

//      for(int batch_img = 0 ; batch_img < M_; batch_img++)
//      {
//          for(int dim_in = 0; dim_in < K_ ; dim_in++ )
//          {
//              for(int dim_out = 0; dim_out < N_; dim_out++)
//              {
//                  caffe_mul<Dtype>(width*height,
//                                   top_diff + width*height*dim_out
//                                   + width*height*N_*batch_img,
//                                   weight_data + width*height*dim_in
//                                   + width*height*K_*dim_out*batch_img,
//                                   weight_times_top_diff);

//                  caffe_add<Dtype>(width*height,
//                                   weight_times_top_diff,
//                                   bottom_data_mutable + width*height*dim_in
//                                   +width*height*K_*batch_img,
//                                   bottom_data_mutable + width*height*dim_in
//                                   +width*height*K_*batch_img);
//              }
//          }

//      }

      const Dtype* weight_data_cpu = this->blobs_[0]->cpu_data();


      /// We probabaly need to add another loop for summation..
      /// TODO: DOUBLE CHECK IF THERE IS A BUG!

      for(int batch_img = 0; batch_img < M_; batch_img++ )
      {
          for(int yy = 0; yy < height ; yy++ )
          {
              for(int xx = 0; xx < width ; xx++ )
              {
                  for(int dim_in = 0; dim_in < Inchannels; dim_in++)
                  {
                      Dtype sum_val =0;

                      for(int dim_out = 0; dim_out < Outchannels; dim_out++)
                      {
                          int idx = batch_img * width * height * Outchannels + yy * width + xx + width * height * dim_out;

                          sum_val += top_diff[idx]*weight_data_cpu[dim_out*Inchannels + dim_in];
                      }

                      int idx_out = batch_img * width * height * Inchannels +  yy * width + xx + width * height * dim_in;

                      bottom_data_mutable[idx_out] = sum_val;
                  }
              }
          }

      }

  }

}




template <typename Dtype>
void InnerProductLayer<Dtype>::Backward_cpu(const vector<Blob<Dtype>*>& top,
    const vector<bool>& propagate_down,
    vector<Blob<Dtype>*>* bottom)
{

  Backward_cpu_dense(top,propagate_down,bottom);
  return;


  const Dtype* top_diff    = top[0]->cpu_diff();
  const Dtype* bottom_data = (*bottom)[0]->cpu_data();

  // Gradient with respect to weight
  caffe_cpu_gemm<Dtype>(CblasTrans, CblasNoTrans, N_, K_, M_, (Dtype)1.,
      top_diff, bottom_data, (Dtype)0., this->blobs_[0]->mutable_cpu_diff());

  if (bias_term_)
  {
    // Gradient with respect to bias
    caffe_cpu_gemv<Dtype>(CblasTrans, M_, N_, (Dtype)1., top_diff,
        reinterpret_cast<const Dtype*>(bias_multiplier_->cpu_data()), (Dtype)0.,
        this->blobs_[1]->mutable_cpu_diff());
  }

  if (propagate_down[0])
  {
    // Gradient with respect to bottom data
    caffe_cpu_gemm<Dtype>(CblasNoTrans, CblasNoTrans, M_, K_, N_, (Dtype)1.,
        top_diff, this->blobs_[0]->cpu_data(), (Dtype)0.,
        (*bottom)[0]->mutable_cpu_diff());
  }
}

INSTANTIATE_CLASS(InnerProductLayer);

}  // namespace caffe

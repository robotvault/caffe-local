// Copyright 2014 BVLC and contributors.

#include <algorithm>
#include <cfloat>
#include <vector>

#include "thrust/device_vector.h"

#include "caffe/layer.hpp"
#include "caffe/vision_layers.hpp"
#include "caffe/util/math_functions.hpp"

#include <boost/math/common_factor_rt.hpp>

using std::max;

namespace caffe {

template <typename Dtype>
__global__ void kernel_get_max(const int num, const int dim,
    const Dtype* data, Dtype* out)
{
  CUDA_KERNEL_LOOP(index, num)
  {
    Dtype maxval = -FLT_MAX;

    for (int i = 0; i < dim; ++i)
    {
      maxval = max(data[index * dim + i], maxval);
    }

    out[index] = maxval;
  }
}


template <typename Dtype>
__global__ void kernel_get_max_dense(const int width,
                                     const int height,
                                     const int dim,
                                     const Dtype* data,
                                     Dtype* out)
{
    uint xx = blockIdx.x*blockDim.x + threadIdx.x;
    uint yy = blockIdx.y*blockDim.y + threadIdx.y;

    Dtype max_val = -FLT_MAX;

    if ( xx < width && yy < height)
    {
        for(int i = 0; i < dim; i++)
        {
            max_val = max(data[yy*width+xx+width*height*i],max_val);
        }

        out[yy*width+xx] = max_val;
    }
}


template <typename Dtype>
__global__ void kernel_softmax_div(const int num, const int dim,
    const Dtype* scale, Dtype* data) {
  CUDA_KERNEL_LOOP(index, num * dim) {
    int n = index / dim;
    data[index] /= scale[n];
  }
}

template <typename Dtype>
__global__ void kernel_softmax_div_dense(const int width,
                                         const int height,
                                         const int dim,
                                         const Dtype* scale,
                                         Dtype* data)
{
    uint xx = blockIdx.x*blockDim.x + threadIdx.x;
    uint yy = blockIdx.y*blockDim.y + threadIdx.y;

    if ( xx < width && yy < height)
    {
        #pragma unroll
        for(int i = 0; i < dim; i++)
        {
            data[yy*width + xx + i * width * height] = data[yy*width + xx + i * width * height] / scale[yy*width+xx];
        }
    }

}


template <typename Dtype>
__global__ void kernel_exp(const int num, const Dtype* data, Dtype* out) {
  CUDA_KERNEL_LOOP(index, num) {
    out[index] = exp(data[index]);
  }
}

//template <typename Dtype>
//__global__ void kernel_exp_dense(const int width,
//                                 const int height,
//                                 const int dim, const Dtype* data, Dtype* out) {
//  CUDA_KERNEL_LOOP(index, num) {
//    out[index] = exp(data[index]);
//  }
//}


template <typename Dtype>
Dtype SoftmaxLayer<Dtype>::Forward_gpu_dense(const vector<Blob<Dtype>*>& bottom,
    vector<Blob<Dtype>*>* top)
{


//  LOG(INFO) <<"Coming here in Forward_gpu_dense";

  const Dtype* bottom_data = bottom[0]->gpu_data();

  Dtype* top_data = (*top)[0]->mutable_gpu_data();

  Dtype* scale_data = scale_.mutable_gpu_data();

  int num = bottom[0]->num();
  int dim = bottom[0]->channels();

  int width  = bottom[0]->width();
  int height = bottom[0]->height();

  caffe_copy(bottom[0]->count(), bottom_data, top_data);

//  LOG(INFO) <<"width = " << width;
//  LOG(INFO) <<"height = " << height;
//  LOG(INFO) <<"dim  = " << dim;
//  LOG(INFO) << "num = " << num;

  // we need to subtract the max to avoid numerical issues, compute the exp,
  // and then normalize.
  // Compute max
  // NOLINT_NEXT_LINE(whitespace/operators)

//  kernel_get_max<Dtype><<<CAFFE_GET_BLOCKS(num), CAFFE_CUDA_NUM_THREADS>>>(
//      num, dim, bottom_data, scale_data);


//  float time;
//  cudaEvent_t start, stop;

//  cudaEventCreate(&start);
//  cudaEventCreate(&stop);
//  cudaEventRecord(start, 0);

//  for(int i = 0; i < num; i++)
//      for(int yy = 0; yy

  Dtype* bottom_cpu = new Dtype[width*height*dim*num];

  cudaMemcpy(bottom_cpu,
             bottom_data,
             width*height*dim*num*sizeof(Dtype),
             cudaMemcpyDeviceToHost);

//  std::cout<<"Bottom data " << std::endl;

//  for(int i = 0; i < num; i++)
//  {
//      for(int yy = 0; yy < height ; yy++ )
//      {
//          for(int xx = 0; xx < width ; xx++ )
//          {
//              for(int d = 0; d < dim; d++)
//              {
//                std::cout<<" "<< bottom_cpu[i*width*height*dim + width*height*d + yy*width+xx];
//              }

//              std::cout<<std::endl;
//          }
//      }

//  }


  for(int i = 0; i < num; i++ )
  {

      const int blockWidthx = 16;
      const int blockWidthy = 16;

      const dim3 dimBlock(boost::math::gcd<unsigned>( width , blockWidthx),
                          boost::math::gcd<unsigned>(height , blockWidthy),
                          1);

      const dim3 dimGrid( width / dimBlock.x,
                          height / dimBlock.y,
                          1);

      kernel_get_max_dense<Dtype><<<dimGrid, dimBlock>>>(width,height,dim,bottom_data+i*width*height*dim,scale_data+i*width*height);
  }

//  cudaEventRecord(stop, 0);
//  cudaEventSynchronize(stop);
//  cudaEventElapsedTime(&time, start, stop);

//  printf("Time to Get the max elements:  %3.1f ms \n", time);

//  std::cout<<"Kernel get max run finally " << std::endl;




  // subtraction
//  caffe_gpu_gemm<Dtype>(CblasNoTrans, CblasNoTrans, num, dim, 1, -1.,
//      scale_data, sum_multiplier_.gpu_data(), 1., top_data);


//   std::cout<<"Before: Printing the top_data_cpu" << std::endl;

//   Dtype* top_data_cpu = (*top)[0]->mutable_cpu_data();

//   for(int batch_img = 0; batch_img < num; batch_img++)
//   {
//       for(int d = 0; d < dim; d++)
//       {
//           for(int yy = 0; yy < height; yy++)
//           {
//               for(int xx =0; xx<width; xx++)
//               {
//                   std::cout<<"["<<xx<<","<<yy<<"] = " << top_data_cpu[batch_img * width * height * dim
//                              + d * width * height + yy * width + xx ];
//               }

//               std::cout<<std::endl;
//           }
//       }
//   }

//  cudaEventCreate(&start);
//  cudaEventCreate(&stop);
//  cudaEventRecord(start, 0);

   for(int batch_img = 0; batch_img < num; batch_img++ )
   {

       /// It is also doing the exponentiation after the subtraction

       caffe_gpu_exp_axpby_dense<Dtype>(width,
                                    height,
                                    dim,
                                    (Dtype)-1.0,
                                    scale_data + batch_img * width * height,
                                    (Dtype)1.0,
                                    top_data   + batch_img * width * height * dim);

   }

//   cudaEventRecord(stop, 0);
//   cudaEventSynchronize(stop);
//   cudaEventElapsedTime(&time, start, stop);

//   printf("Time to subtract the max element:  %3.1f ms \n", time);

//   std::cout<<"After Subtraction: Printing the top_data_cpu" << std::endl;

//   Dtype* top_data_cpu_1 = (*top)[0]->mutable_cpu_data();

//   for(int batch_img = 0; batch_img < num; batch_img++)
//   {
//       for(int yy = 0; yy < height; yy++)
//       {
//           for(int xx =0; xx<width; xx++)
//           {
//               std::cout<<"["<<xx<<","<<yy<<"] = ";
//               for(int d = 0; d < dim; d++)
//               {

//                  std::cout <<" " << top_data_cpu_1[batch_img * width * height * dim
//                              + d * width * height + yy * width + xx ];
//               }

//               std::cout<<std::endl;
//           }
//       }
//   }

//   std::cout<<"Did the subtraction in the gpu" << std::endl;


  // Perform exponentiation
  // NOLINT_NEXT_LINE(whitespace/operators)

//   cudaEventCreate(&start);
//   cudaEventCreate(&stop);
//   cudaEventRecord(start, 0);


//  for(int i = 0; i < num; i++)
//  {
//      kernel_exp<Dtype><<<CAFFE_GET_BLOCKS(width * height* dim), CAFFE_CUDA_NUM_THREADS>>>(
//         width * height * dim, top_data + i*width*height*dim, top_data + i*width*height*dim);
//  }

//  cudaEventRecord(stop, 0);
//  cudaEventSynchronize(stop);
//  cudaEventElapsedTime(&time, start, stop);

//  printf("Time to do exponentiation:  %3.1f ms \n", time);



//  LOG(INFO) <<"Exponentiation done";


  // sum after exp
//  caffe_gpu_gemv<Dtype>(CblasNoTrans, num, dim, 1., top_data,
//      sum_multiplier_.gpu_data(), 0., scale_data);


//  cudaEventCreate(&start);
//  cudaEventCreate(&stop);
//  cudaEventRecord(start, 0);


  for(int batch_img = 0; batch_img < num; batch_img++)
  {

//      Dtype *sum_exp;
//      cudaMalloc((void **)&sum_exp,width*height*sizeof(Dtype));

//      caffe_gpu_set<Dtype>(width*height,(Dtype)0.0,sum_exp);

//      for(int d = 0; d < dim; d++ )
//      {

//          caffe_gpu_add<Dtype>(width*height,
//                               top_data + batch_img * width * height * dim + d * width * height,
//                               sum_exp,
//                               sum_exp);
//      }

//      cudaMemcpy(scale_data + batch_img * width * height,
//                 sum_exp,
//                 width*height*sizeof(Dtype),
//                 cudaMemcpyDeviceToDevice);

//      cudaFree(sum_exp);


      /// Sum up the exponentiated values and then divide each by this sum.


      caffe_gpu_sum_and_div_dense<Dtype>(width,
                                         height,
                                         dim,
                                         scale_data + batch_img * width * height,
                                         top_data + batch_img * width * height * dim);

  }

//  cudaEventRecord(stop, 0);
//  cudaEventSynchronize(stop);
//  cudaEventElapsedTime(&time, start, stop);

//  printf("Time to do sum the exponentials:  %3.1f ms \n", time);

//  Dtype* scale_data_cpu = scale_.mutable_cpu_data();

//  std::cout<<"Printing Scale data - sum_exp" << std::endl;

//  for(int batch_img = 0; batch_img < num; batch_img++)
//  {
//      std::cout<<"batch_img = " << batch_img << std::endl;

//      for(int yy = 0; yy < height; yy++)
//      {
//          for(int xx =0; xx<width; xx++)
//          {
//              std::cout<<"["<<xx<<","<<yy<<"] = " << scale_data_cpu[batch_img * width * height + yy * width + xx ];
//          }

//          std::cout<<std::endl;
//      }
//  }






//  LOG(INFO)<<"Sum after the exponentiation";

  // Do division
  // NOLINT_NEXT_LINE(whitespace/operators)
//  kernel_softmax_div<Dtype><<<CAFFE_GET_BLOCKS(num * dim),
//                              CAFFE_CUDA_NUM_THREADS>>>(
//      num, dim, scale_data, top_data);

//  cudaEventCreate(&start);
//  cudaEventCreate(&stop);
//  cudaEventRecord(start, 0);

//  for(int batch_img = 0; batch_img < num; batch_img++ )
//  {

//      const int blockWidthx = 32;
//      const int blockWidthy = 32;

//      const dim3 dimBlock(boost::math::gcd<unsigned>( width , blockWidthx),
//                          boost::math::gcd<unsigned>(height , blockWidthy),
//                          1);

//      const dim3 dimGrid( width / dimBlock.x,
//                         height / dimBlock.y,
//                         1);

//      kernel_softmax_div_dense<Dtype><<<dimGrid, dimBlock>>>(
//                                  width,
//                                  height,
//                                  dim,
//                                  scale_data + width * height * batch_img,
//                                  top_data + batch_img * width * height * dim);

//  }


//  cudaEventRecord(stop, 0);
//  cudaEventSynchronize(stop);
//  cudaEventElapsedTime(&time, start, stop);

//  printf("Time to do do the divison:  %3.1f ms \n", time);


//  std::cout << "After Divison: " << std::endl;

//  Dtype* top_data_cpu = (*top)[0]->mutable_cpu_data();

//  for(int batch_img = 0; batch_img < num; batch_img++)
//  {
//      for(int yy = 0; yy < height; yy++)
//      {
//          for(int xx =0; xx<width; xx++)
//          {
//              std::cout<<"["<<xx<<","<<yy<<"] = ";
//              for(int d = 0; d < dim; d++)
//              {

//                  std::cout <<" " << top_data_cpu[batch_img * width * height * dim
//                              + d * width * height + yy * width + xx ];
//              }

//              std::cout<<std::endl;
//          }
//      }
//  }

//  LOG(INFO)<<"The divison" ;



//  std::cout<<"Printing the top_data_cpu" << std::endl;

//  Dtype* top_data_cpu = (*top)[0]->mutable_cpu_data();

//  for(int batch_img = 0; batch_img < num; batch_img++)
//  {
//      for(int d = 0; d < dim; d++)
//      {
//          for(int yy = 0; yy < height; yy++)
//          {
//              for(int xx =0; xx<width; xx++)
//              {
//                  std::cout<<"["<<xx<<","<<yy<<"] = " << top_data_cpu[batch_img * width * height * dim
//                                                                + d * width * height + yy * width + xx ];
//              }

//              std::cout<<std::endl;
//          }
//      }
//  }


  return Dtype(0);
}





template <typename Dtype>
Dtype SoftmaxLayer<Dtype>::Forward_gpu(const vector<Blob<Dtype>*>& bottom,
    vector<Blob<Dtype>*>* top)
{

  return Forward_gpu_dense(bottom,top);

  const Dtype* bottom_data = bottom[0]->gpu_data();
  Dtype* top_data = (*top)[0]->mutable_gpu_data();
  Dtype* scale_data = scale_.mutable_gpu_data();
  int num = bottom[0]->num();
  int dim = bottom[0]->count() / bottom[0]->num();
  caffe_copy(bottom[0]->count(), bottom_data, top_data);
  // we need to subtract the max to avoid numerical issues, compute the exp,
  // and then normalize.
  // Compute max
  // NOLINT_NEXT_LINE(whitespace/operators)
  kernel_get_max<Dtype><<<CAFFE_GET_BLOCKS(num), CAFFE_CUDA_NUM_THREADS>>>(
      num, dim, bottom_data, scale_data);
  // subtraction
  caffe_gpu_gemm<Dtype>(CblasNoTrans, CblasNoTrans, num, dim, 1, -1.,
      scale_data, sum_multiplier_.gpu_data(), 1., top_data);
  // Perform exponentiation
  // NOLINT_NEXT_LINE(whitespace/operators)
  kernel_exp<Dtype><<<CAFFE_GET_BLOCKS(num * dim), CAFFE_CUDA_NUM_THREADS>>>(
      num * dim, top_data, top_data);
  // sum after exp
  caffe_gpu_gemv<Dtype>(CblasNoTrans, num, dim, 1., top_data,
      sum_multiplier_.gpu_data(), 0., scale_data);
  // Do division
  // NOLINT_NEXT_LINE(whitespace/operators)
  kernel_softmax_div<Dtype><<<CAFFE_GET_BLOCKS(num * dim),
                              CAFFE_CUDA_NUM_THREADS>>>(
      num, dim, scale_data, top_data);
  return Dtype(0);
}



template <typename Dtype>
void SoftmaxLayer<Dtype>::Backward_gpu_dense(
        const vector<Blob<Dtype>*>& top,
        const vector<bool>& propagate_down,
        vector<Blob<Dtype>*>* bottom)
{
  const Dtype* top_diff = top[0]->gpu_diff();
  const Dtype* top_data = top[0]->gpu_data();

  Dtype* bottom_diff = (*bottom)[0]->mutable_gpu_diff();
  Dtype* scale_data  = scale_.mutable_gpu_data();

  int num    = top[0]->num();
  int dim    = top[0]->channels(); //top[0]->count() / top[0]->num();
  int width  = top[0]->width();
  int height = top[0]->height();

  LOG(INFO)<<"\n xxxxxxxxxxxxxx t.height   = " << height
           <<"\n xxxxxxxxxxxxxx t.width    = " << width
           <<"\n xxxxxxxxxxxxxx t.num      = " << num
           <<"\n xxxxxxxxxxxxxx t.channels = " << top[0]->channels()
           <<"\n xxxxxxxxxxxxxx t.count    = " << top[0]->count()
           <<"\n xxxxxxxxxxxxxx t.dmcounted = " << top[0]->count() / top[0]->num();


  LOG(INFO)<<"Bakward propagation in Softmax Layer: Hit Enter";
//  getchar();

  caffe_copy(top[0]->count(), top_diff, bottom_diff);

  Dtype* top_diff_cpu = top[0]->mutable_cpu_diff();

  std::cout<<"top_diff(cpu)" << std::endl;

  for(int i = 0; i < num; i++)
  {
      for(int yy =0; yy < height ; yy++)
      {
          for(int xx = 0; xx < width ; xx++)
          {
              std::cout<<" " << top_diff_cpu[yy*width+xx+i*width*height];
          }
      }
      std::cout<<std::endl;
  }


  /// Compute inner1d(top_diff, top_data) and subtract them from the bottom diff

  caffe_gpu_set<Dtype>(width*height*num,Dtype(0.0),scale_data);


  for(int batch_img = 0; batch_img < num; batch_img++ )
  {

      caffe_gpu_mul_and_add_dense<Dtype>(width,
                                         height,
                                         dim,
                                         top_diff + batch_img * width * height * dim,
                                         top_data + batch_img * width * height * dim,
                                         scale_data + batch_img * width * height);
  }

  std::cout<<"scale_data in backward pass" << std::endl;

  Dtype* scale_data_cpu = scale_.mutable_cpu_data();

  for(int i = 0; i < num; i++)
  {
      for(int yy =0; yy < height ; yy++)
      {
          for(int xx = 0; xx < width ; xx++)
          {
              std::cout<<" " << scale_data_cpu[yy*width+xx+i*width*height];
          }
      }
      std::cout<<std::endl;
  }

  for(int batch_img = 0; batch_img < num; batch_img++)
  {

      caffe_gpu_axpby_dense<Dtype>(width,
                                   height,
                                   dim,
                                   (Dtype)-1.0,
                                   scale_data  + batch_img * width * height,
                                   (Dtype)1.0,
                                   bottom_diff + batch_img * width * height * dim);
  }

  /// elementwise multiplication
//  caffe_mul<Dtype>(top[0]->count(), bottom_diff, top_data, bottom_diff);
  caffe_gpu_mul<Dtype>(top[0]->count(), bottom_diff, top_data, bottom_diff);



  Dtype* bottom_diff_cpu = (*bottom)[0]->mutable_cpu_diff();

  std::cout<<"bottom_diff(cpu)" << std::endl;

  for(int i = 0; i < num; i++)
  {
      for(int yy =0; yy < height ; yy++)
      {
          for(int xx = 0; xx < width ; xx++)
          {
              for(int d = 0; d < dim ; d++)
                  std::cout<<" " << bottom_diff_cpu[yy*width+xx+i*width*height*dim + width*height*d];

              std::cout<<std::endl;
          }
      }

  }


}



// TODO(Yangqing): implement the GPU version of softmax.
template <typename Dtype>
void SoftmaxLayer<Dtype>::Backward_gpu(const vector<Blob<Dtype>*>& top,
    const vector<bool>& propagate_down, vector<Blob<Dtype>*>* bottom)
{

  return Backward_gpu_dense(top,propagate_down,bottom);

//  std::cout<<"Here we are in backward_gpu of SoftMaxLayer" << std::endl;

  const Dtype* top_diff = top[0]->gpu_diff();
  const Dtype* top_data = top[0]->gpu_data();
  Dtype* bottom_diff = (*bottom)[0]->mutable_gpu_diff();
  int num = top[0]->num();
  int dim = top[0]->count() / top[0]->num();
  caffe_copy(top[0]->count(), top_diff, bottom_diff);
  // Compute inner1d(top_diff, top_data) and subtract them from the bottom diff
  // cuda dot returns the result to cpu, so we temporarily change the pointer
  // mode
  CUBLAS_CHECK(cublasSetPointerMode(Caffe::cublas_handle(),
      CUBLAS_POINTER_MODE_DEVICE));
  Dtype* scale_data = scale_.mutable_gpu_data();
  for (int i = 0; i < num; ++i) {
    caffe_gpu_dot<Dtype>(dim, top_diff + i * dim,
        top_data + i * dim, scale_data + i);
  }
  CUBLAS_CHECK(cublasSetPointerMode(Caffe::cublas_handle(),
      CUBLAS_POINTER_MODE_HOST));
  // subtraction
  caffe_gpu_gemm<Dtype>(CblasNoTrans, CblasNoTrans, num, dim, 1, -1.,
      scale_.gpu_data(), sum_multiplier_.gpu_data(), 1., bottom_diff);
  // elementwise multiplication
  caffe_gpu_mul<Dtype>(top[0]->count(), bottom_diff, top_data, bottom_diff);
}

INSTANTIATE_CLASS(SoftmaxLayer);


}  // namespace caffe

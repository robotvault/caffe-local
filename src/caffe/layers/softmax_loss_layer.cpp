// Copyright 2014 BVLC and contributors.

#include <algorithm>
#include <cfloat>
#include <vector>

#include "caffe/layer.hpp"
#include "caffe/vision_layers.hpp"
#include "caffe/util/math_functions.hpp"

#include <iostream>

using std::max;


namespace caffe {

template <typename Dtype>
void SoftmaxWithLossLayer<Dtype>::SetUp(const vector<Blob<Dtype>*>& bottom,
      vector<Blob<Dtype>*>* top)
{
  Layer<Dtype>::SetUp(bottom, top);

  softmax_bottom_vec_.clear();
  softmax_bottom_vec_.push_back(bottom[0]);

  softmax_top_vec_.push_back(&prob_);
  softmax_layer_->SetUp(softmax_bottom_vec_, &softmax_top_vec_);

  if (top->size() >= 1)
  {
    /// softmax loss (averaged across batch)
    (*top)[0]->Reshape(1, 1, 1, 1);
  }

  if (top->size() == 2)
  {
     /// softmax output
    (*top)[1]->Reshape(bottom[0]->num(),
                       bottom[0]->channels(),
                       bottom[0]->height(),
                       bottom[0]->width());

      LOG(INFO) << "output label size: "
                << (*top)[1]->num() << ","
                << (*top)[1]->channels() << ","
                << (*top)[1]->height() << ","
                << (*top)[1]->width();
  }



}

template <typename Dtype>
Dtype SoftmaxWithLossLayer<Dtype>::Forward_cpu_dense(const vector<Blob<Dtype>*>& bottom,
                                                           vector<Blob<Dtype>*>* top)
{

  std::cout<<"Foward passing the softmax layer" << std::endl;

  /// The forward pass computes the softmax prob values.
  softmax_bottom_vec_[0] = bottom[0];
  softmax_layer_->Forward(softmax_bottom_vec_, &softmax_top_vec_);

  const Dtype* prob_data = prob_.cpu_data();
  const Dtype* label     = bottom[1]->cpu_data();

  int num    = prob_.num();
  int dim    = prob_.channels();
  int width  = prob_.width();
  int height = prob_.height();

  Dtype loss = 0;

  /// Here I think num is the total number of images in a batch

  LOG(INFO)<<"Going to compute the loss";

  for (int i = 0; i < num; ++i)
  {
       /// We might need to change it to something like
       for(int xx = 0 ; xx < width; xx++)
       {
           for(int yy = 0 ; yy < height ; yy++)
           {
              int idx = i*width*height + width * yy + xx;

              loss += -log(max(prob_data[xx+width*yy+width*height*static_cast<int>(label[idx]) + i*width*height*dim],
                           Dtype(FLT_MIN)));

              if ( isnan(loss) || isinf(loss) )
              {
                  LOG(INFO) <<"loss = " << loss << " prob_data = " << prob_data[xx+width*yy+width*height*static_cast<int>(label[idx]) + i*width*height*dim]
                            <<" label[idx] = " << label[idx];
                  LOG(INFO) << "width = " << width<<" height = " << height << ", xx = " << xx << ", yy = " << yy << "idx = " << idx
                            << " i = " << i;
                  getchar();
              }
           }
       }

  }

  std::cout<<"Computed the total loss function value at the pass" << std::endl;

  if (top->size() >= 1)
  {
    (*top)[0]->mutable_cpu_data()[0] = loss / (num * width * height );
  }

  if (top->size() == 2)
  {
    (*top)[1]->ShareData(prob_);
  }

  return loss / (num * width * height );
}

template <typename Dtype>
Dtype SoftmaxWithLossLayer<Dtype>::Forward_cpu(
    const vector<Blob<Dtype>*>& bottom, vector<Blob<Dtype>*>* top)
{
  /// The forward pass computes the softmax prob values.

  return Forward_cpu_dense(bottom,top);


  std::cout << "Entering here ***********************************" << std::endl;

  softmax_bottom_vec_[0] = bottom[0];
  softmax_layer_->Forward(softmax_bottom_vec_, &softmax_top_vec_);

  const Dtype* prob_data = prob_.cpu_data();
  const Dtype* label = bottom[1]->cpu_data();

  int num = prob_.num();
  int dim = prob_.count() / num;

  Dtype loss = 0;

  for (int i = 0; i < num; ++i)
  {
    loss += -log(max(prob_data[i * dim + static_cast<int>(label[i])],
                     Dtype(FLT_MIN)));
  }

  if (top->size() >= 1) {
    (*top)[0]->mutable_cpu_data()[0] = loss / num;
  }

  if (top->size() == 2) {
    (*top)[1]->ShareData(prob_);
  }

  return loss / num;
}



/// http://stats.stackexchange.com/questions/79454/softmax-layer-in-a-neural-network
template <typename Dtype>
void SoftmaxWithLossLayer<Dtype>::Backward_cpu_dense(
        const vector<Blob<Dtype>*>& top,
        const vector<bool>& propagate_down,
        vector<Blob<Dtype>*>* bottom)
{
  if (propagate_down[1])
  {
    LOG(FATAL) << this->type_name()
               << " Layer cannot backpropagate to label inputs.";
  }

  LOG(INFO)<<"Doing backward pass for the softmax layer with loss";

  if (propagate_down[0])
  {
      LOG(INFO) <<"propagating down  the derivatives";

      Dtype* bottom_diff = (*bottom)[0]->mutable_cpu_diff();

      const Dtype* prob_data = prob_.cpu_data();

      caffe_copy(prob_.count(), prob_data, bottom_diff);

      const Dtype* label = (*bottom)[1]->cpu_data();

      int num    = prob_.num();
      int dim    = prob_.channels();///prob_.count() / num;
      int width  = prob_.width();
      int height = prob_.height();


      LOG(INFO)<<"bottom_diff.num      = " << (*bottom)[0]->num();
      LOG(INFO)<<"bottom_diff.channels = " << (*bottom)[0]->channels();
      LOG(INFO)<<"bottom_diff.width    = " << (*bottom)[0]->width();
      LOG(INFO)<<"bottom_diff.height   = " << (*bottom)[0]->height();


      /// Subtract from only that dimension where you computed the loss from..
      for (int i = 0; i < num; ++i)
      {
          for(int xx = 0 ; xx < width ; xx++)
          {
               for(int yy = 0 ; yy < height ; yy++)
               {
                   int idx = xx + yy * width + i * width * height;

                   bottom_diff[i * dim * height * width + xx + width*yy + height * width * static_cast<int>(label[idx])] -= 1;
               }
          }

      }

      /// Scale down gradient
      caffe_scal(prob_.count(), Dtype(1) / (num * width * height ), bottom_diff);
  }

  LOG(INFO) <<"Finished the backward pass for the layer";
}


template <typename Dtype>
void SoftmaxWithLossLayer<Dtype>::Backward_cpu(const vector<Blob<Dtype>*>& top,
                                               const vector<bool>& propagate_down,
                                                     vector<Blob<Dtype>*>* bottom)
{

    Backward_cpu_dense(top,propagate_down,bottom);
    return;


  if (propagate_down[1])
  {
    LOG(FATAL) << this->type_name()
               << " Layer cannot backpropagate to label inputs.";
  }

  if (propagate_down[0])
  {
    Dtype* bottom_diff = (*bottom)[0]->mutable_cpu_diff();
    const Dtype* prob_data = prob_.cpu_data();

    caffe_copy(prob_.count(), prob_data, bottom_diff);

    const Dtype* label = (*bottom)[1]->cpu_data();

    int num = prob_.num();
    int dim = prob_.count() / num;

    for (int i = 0; i < num; ++i)
    {
      bottom_diff[i * dim + static_cast<int>(label[i])] -= 1;
    }

    // Scale down gradient
    caffe_scal(prob_.count(), Dtype(1) / num, bottom_diff);
  }
}


INSTANTIATE_CLASS(SoftmaxWithLossLayer);


}  // namespace caffe

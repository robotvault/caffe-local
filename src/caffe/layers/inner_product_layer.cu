// Copyright 2014 BVLC and contributors.

#include <cublas_v2.h>

#include <vector>

#include "caffe/blob.hpp"
#include "caffe/common.hpp"
#include "caffe/filler.hpp"
#include "caffe/layer.hpp"
#include "caffe/vision_layers.hpp"
#include "caffe/util/math_functions.hpp"

#include <thrust/reduce.h>
#include <thrust/device_vector.h>

namespace caffe {


template <typename Dtype>
Dtype InnerProductLayer<Dtype>::Forward_gpu_dense(const vector<Blob<Dtype>* >& bottom,
                                                  vector<Blob<Dtype>* >* top)
{
    const Dtype* bottom_data = bottom[0]->gpu_data();

    Dtype* top_data = (*top)[0]->mutable_gpu_data();

    const Dtype* weight = this->blobs_[0]->gpu_data();

    const int w_width   = this->blobs_[0]->width();
    const int w_height  = this->blobs_[0]->height();

    const int width  = bottom[0]->width();
    const int height = bottom[0]->height();

//    std::cout<<"w_width = " << w_width <<" , w_height = " << w_height << std::endl;

    int Outchannels = N_;
    int Inchannels  = K_;


    for(int b_images = 0; b_images < M_ ; b_images++)
    {
        caffe_gpu_AXpB<Dtype>(weight,
                            w_width,
                            w_height,
                            this->blobs_[1]->gpu_data(),
                            bottom_data + b_images * width * height * Inchannels ,
                            width,
                            height,
                            top_data + b_images * width * height * Outchannels ,
                            b_images);

    }

//    if ( bias_term_ )
//    {
//        for(int batch_img = 0; batch_img < M_ ; batch_img++)
//        {
//            caffe_gpu_add<Dtype>(width*height*Outchannels,
//                             this->blobs_[1]->gpu_data(),
//                             top_data + batch_img*width*height*Outchannels,
//                             top_data + batch_img*width*height*Outchannels);
//        }
//    }

    return Dtype(0.0);

}



template <typename Dtype>
void InnerProductLayer<Dtype>::Backward_gpu_dense(const vector<Blob<Dtype>*>& top,
                                                  const vector<bool>& propagate_down,
                                                  vector<Blob<Dtype>*>* bottom)
{


  LOG(INFO)<<"Backward GPU dense: InnerProductLayer";

  const Dtype* top_diff    = top[0]->gpu_diff();
  const Dtype* bottom_data = (*bottom)[0]->gpu_data();


  int width  = (*bottom)[0]->width();
  int height = (*bottom)[0]->height();

  int Outchannels = N_;
  int Inchannels  = K_;

  Dtype* blob_1_gpu_data = this->blobs_[1]->mutable_gpu_diff();
  Dtype* weight_data     = this->blobs_[0]->mutable_gpu_diff();
  const Dtype* weight_data_prev = this->blobs_[0]->gpu_data();
  Dtype* blob_1_cpu_data = new Dtype[Outchannels];


  int t_width     = top[0]->width();
  int t_height    = top[0]->height();
  int t_channels  = top[0]->channels();


  int b_width     = this->blobs_[1]->width();
  int b_height    = this->blobs_[1]->height();
  int b_channels  = this->blobs_[1]->channels();



//  LOG(INFO)<<"t_width = " << t_width <<", t_height = "<< t_height <<", t_channels = "<< t_channels;
//  LOG(INFO)<<"bias_width = " << b_width <<", bias_height = "<< b_height <<", bias_channels = "<< b_channels;


//  Dtype* bottom_data_cpu = (*botto)


  /// Gradient with respect to weight

  /// TODO: double check if there is any scale factor we need to divide
  /// the data by.

  Dtype* mul_top_diff;
  cudaMalloc((void **) &mul_top_diff, (width * height * sizeof(Dtype)));
  caffe_gpu_set<Dtype>(width * height,(Dtype)(0.0),mul_top_diff);



  int w_width  = Inchannels;
  int w_height = Outchannels;

  Dtype* weight_data_cpu = new Dtype[Outchannels*Inchannels];

  Dtype* mul_top_diff_cpu = new Dtype[width*height];

  for(int dim_out = 0; dim_out < Outchannels; dim_out++ )
  {
      for(int dim_in = 0; dim_in < Inchannels ; dim_in++ )
      {
          Dtype sum_mul_top_diff = 0;

          for(int batch_img = 0; batch_img < M_; batch_img++ )
          {
              caffe_gpu_mul<Dtype>(width*height,
                                  top_diff + width*height*dim_out
                                  + width*height*Outchannels*batch_img,
                                  bottom_data + width*height*dim_in
                                  + width*height*Inchannels*batch_img,
                                  mul_top_diff);

              //std::cout<<"Done the multiplication" << std::endl;

              thrust::device_ptr<Dtype> cptr = thrust::device_pointer_cast(mul_top_diff);

              sum_mul_top_diff += (Dtype)(thrust::reduce(cptr,cptr+width*height,(Dtype)(0.0),thrust::plus<Dtype>()));
//              std::cout<<"[thrust sum" << sum_mul_top_diff<<"] ";

//              cudaMemcpy(mul_top_diff_cpu,mul_top_diff,width*height*sizeof(Dtype),cudaMemcpyDeviceToHost);

//              for(int i = 0; i < width*height;i++)
//                  sum_mul_top_diff+= mul_top_diff_cpu[i];

              //std::cout<<"Done the summation" << std::endl;
          }

          weight_data_cpu[dim_out * Inchannels + dim_in ] = sum_mul_top_diff;

//          std::cout<<" " << weight_data_cpu[dim_out * Inchannels + dim_in ];
      }

//      std::cout<<std::endl;
  }

  delete mul_top_diff_cpu;

//  getchar();

  cudaMemcpy(weight_data,weight_data_cpu,Outchannels*Inchannels*sizeof(Dtype),cudaMemcpyHostToDevice);

  cudaFree(mul_top_diff);



  delete weight_data_cpu;

  LOG(INFO) <<"Arriving here at the end of sum_mul_top_diff ";

  if (bias_term_)
  {
    ///  Gradient with respect to bias
    ///  NAME
    ///  DGEMV - perform one of the matrix-vector operations   y :=
    ///  alpha*A*x + beta*y, or y := alpha*A'*x + beta*y,

    ///      SUBROUTINE DGEMV ( TRANS, M, N, ALPHA, A, LDA, X, INCX,
    ///                       BETA, Y, INCY )

      /// TODO: double check if the averaging is done or not
      /// i.e. Do we need scaling here by 1/batch_size?
      /// Need to change it to transpose?

      LOG(INFO)<<"Coming to the start of bias gradients";

      for(int dim_out = 0; dim_out < Outchannels; dim_out++)
      {

          Dtype sum_total = (Dtype)(0.0);

          Dtype* sum_top_diff;
          cudaMalloc((void**)&sum_top_diff,width*height*sizeof(Dtype));
          caffe_gpu_set<Dtype>(width*height,(Dtype)(0.0),sum_top_diff);

          for(int batch_img = 0 ; batch_img < M_; batch_img++ )
          {
              caffe_gpu_add<Dtype>(width*height,
                                  top_diff + width*height*dim_out
                                  + Outchannels*width*height*batch_img,
                                  sum_top_diff,
                                  sum_top_diff);
          }

          thrust::device_ptr<Dtype> cptr = thrust::device_pointer_cast(sum_top_diff);

          sum_total = thrust::reduce(cptr,cptr+width*height,(Dtype)(0.0),thrust::plus<Dtype>());

          blob_1_cpu_data[dim_out] = sum_total;

          cudaFree(sum_top_diff);
      }

      cudaMemcpy(blob_1_gpu_data,blob_1_cpu_data,Outchannels*sizeof(Dtype),cudaMemcpyHostToDevice);

      delete blob_1_cpu_data;

//      LOG(INFO)<<"Coming to the end of bias gradients";
  }

  if (propagate_down[0])
  {
    /// Gradient with respect to bottom data

      Dtype* bottom_data_mutable = (*bottom)[0]->mutable_gpu_diff();

      for(int batch_img = 0; batch_img < M_; batch_img++ )
      {

          caffe_gpu_AX<Dtype>(weight_data_prev,
                              w_width,
                              w_height,
                              top_diff + batch_img * width * height * Outchannels,
                              width,
                              height,
                              bottom_data_mutable + batch_img * width * height * Inchannels,
                              batch_img);



      }

  }

}

//template <typename Dtype>
//void InnerProductLayer<Dtype>::Backward_cpu_dense(const vector<Blob<Dtype>*>& top,
//                                                  const vector<bool>& propagate_down,
//                                                        vector<Blob<Dtype>*>* bottom)
//{

//  const Dtype* top_diff    = top[0]->cpu_diff();
//  const Dtype* bottom_data = (*bottom)[0]->cpu_data();


//  int width  = (*bottom)[0]->width();
//  int height = (*bottom)[0]->height();

//  Dtype* blob_1_cpu_data = this->blobs_[1]->mutable_cpu_diff(); /// bias
//  Dtype* weight_data     = this->blobs_[0]->mutable_cpu_diff(); /// weight


//  int t_width  = top[0]->width();
//  int t_height = top[0]->height();
//  int t_channels = top[0]->channels();


//  int b_width    = this->blobs_[1]->width();
//  int b_height   = this->blobs_[1]->height();
//  int b_channels = this->blobs_[1]->channels();



////  LOG(INFO)<<"t_width = " << t_width <<", t_height = "<< t_height <<", t_channels = "<< t_channels;
////  LOG(INFO)<<"bias_width = " << b_width <<", bias_height = "<< b_height <<", bias_channels = "<< b_channels;


//  /// Gradient with respect to weight
//  //  caffe_cpu_gemm<Dtype>(CblasTrans,
//  //                        CblasNoTrans,
//  //                        N_, K_, M_,
//  //                        (Dtype)1.,
//  //                        top_diff,
//  //                        bottom_data,
//  //                        (Dtype)0.,
//  //                        this->blobs_[0]->mutable_cpu_diff());

//  /// TODO: double check if there is any scale factor we need to divide
//  /// the data by.

//  Dtype* mul_top_diff = new Dtype[width*height];

//  int Outchannels = N_;
//  int Inchannels  = K_;

//  for(int dim_out = 0; dim_out < Outchannels; dim_out++ )
//  {
//      for(int dim_in = 0; dim_in < Inchannels ; dim_in++ )
//      {
//          Dtype sum_mul_top_diff = 0;

//          for(int batch_img = 0; batch_img < M_; batch_img++ )
//          {
//              for(int i = 0; i < width*height; i++)
//                  mul_top_diff[i] = (Dtype)(0.0);

//              caffe_mul<Dtype>(width*height,
//                               top_diff    + width*height*Outchannels*batch_img
//                               + width*height*dim_out,
//                               bottom_data + width*height*Inchannels *batch_img
//                               + width*height*dim_in,
//                               mul_top_diff);

////              caffe_add<Dtype>(width*height,
////                               mul_top_diff,
////                               weight_data + width*height*dim_in
////                               + width*height*Inchannels*dim_out,
////                               weight_data + width*height*dim_in
////                               + width*height*Inchannels*dim_out);

//              for(int yy = 0; yy < height ; yy++ )
//              {
//                  for(int xx = 0; xx < width; xx++ )
//                  {
//                      sum_mul_top_diff += mul_top_diff[yy*width+xx];
//                  }
//              }

////              sum_mul_top_diff += std::accumulate(mul_top_diff,mul_top_diff+width*height,0);

////              if ( sum_mul_top_diff )
////              std::cout<<"sum_mul_top_diff = " << sum_mul_top_diff<<std::endl;

//          }

//          weight_data[dim_out * Inchannels + dim_in ] = sum_mul_top_diff;
//      }
//  }

//  delete mul_top_diff;

//  LOG(INFO) <<"Arriving here at the end of sum_mul_top_diff ";

//  std::cout<<"weight_data " << std::endl;
//  for(int dim_out = 0; dim_out < Outchannels; dim_out++ )
//  {
//      for(int dim_in = 0; dim_in < Inchannels ; dim_in++ )
//      {
//          std::cout<<" " << weight_data[dim_out * Inchannels + dim_in ];
//      }
//      std::cout<<std::endl;
//  }

//  std::cout<<"top diff data" << std::endl;
//  for(int batch_img = 0; batch_img < M_; batch_img++ )
//  {
//      for(int yy = 0; yy < height ; yy++ )
//      {
//          for(int xx = 0; xx < width; xx++ )
//          {
//              for(int dim_out = 0; dim_out < Outchannels; dim_out++ )
//              {
//                  int idx = batch_img * width * height * Outchannels + width*height*dim_out + yy * width + xx;

//                  std::cout<<" " << top_diff[idx];
//              }

//              std::cout<<std::endl;
//          }
//      }
//  }

//  std::cout<<"bottom data" << std::endl;
//  for(int batch_img = 0; batch_img < M_; batch_img++ )
//  {
//      for(int yy = 0; yy < height ; yy++ )
//      {
//          for(int xx = 0; xx < width; xx++ )
//          {
//              for(int dim_in = 0; dim_in < Inchannels; dim_in++ )
//              {
//                  int idx = batch_img * width * height * Inchannels + width*height*dim_in + yy * width + xx;

//                  std::cout<<" " << bottom_data[idx];
//              }

//              std::cout<<std::endl;
//          }
//      }
//  }

////  for(int batch_img = 0; batch_img < M_; batch_img++ )



//  if (bias_term_)
//  {
//    ///  Gradient with respect to bias
//    ///  NAME
//    ///  DGEMV - perform one of the matrix-vector operations   y :=
//    ///  alpha*A*x + beta*y, or y := alpha*A'*x + beta*y,

//    ///      SUBROUTINE DGEMV ( TRANS, M, N, ALPHA, A, LDA, X, INCX,
//    ///                       BETA, Y, INCY )

//      /// TODO: double check if the averaging is done or not
//      /// i.e. Do we need scaling here by 1/batch_size?
//      /// Need to change it to transpose?

////      LOG(INFO)<<"Coming to the start of bias gradients";

//      Dtype* sum_top_diff = new Dtype[width*height];

//      for(int dim_out = 0; dim_out < Outchannels; dim_out++)
//      {
//          for(int i = 0; i < width * height ; i++)
//              sum_top_diff[i] = (Dtype)(0.0);

//          for(int batch_img = 0 ; batch_img < M_; batch_img++ )
//          {
//              caffe_add<Dtype>(width*height,
//                               top_diff + width*height*dim_out
//                               + Outchannels*width*height*batch_img,
//                               sum_top_diff,
//                               sum_top_diff);
//          }

////          caffe_copy<Dtype>(width*height,
////                            sum_top_diff,
////                            blob_1_cpu_data + dim_out*width*height);
//          //blob_1_cpu_data[dim_out] = std::accumulate(sum_top_diff,sum_top_diff+width*height,0);

//          Dtype sum_total = (Dtype)(0.0);
//          for(int i = 0; i < width*height; i++)
//              sum_total += sum_top_diff[i];

//          blob_1_cpu_data[dim_out] = sum_total;

//      }

//      delete sum_top_diff;


////      LOG(INFO)<<"Coming to the end of bias gradients";
//  }

//  if (propagate_down[0])
//  {

//      std::cout<<"Is going to propagage down" << std::endl;
////      getchar();

//    /// Gradient with respect to bottom data
////    caffe_cpu_gemm<Dtype>(CblasNoTrans,
////                          CblasNoTrans,
////                          M_, K_, N_,
////                          (Dtype)1.,
////                          top_diff,
////                          this->blobs_[0]->cpu_data(),
////                          (Dtype)0.,
////                          (*bottom)[0]->mutable_cpu_diff());


//      Dtype* bottom_data_mutable = (*bottom)[0]->mutable_cpu_diff();

////      for(int batch_img = 0 ; batch_img < M_; batch_img++)
////      {
////          for(int dim_in = 0; dim_in < K_ ; dim_in++ )
////          {
////              for(int dim_out = 0; dim_out < N_; dim_out++)
////              {
////                  caffe_mul<Dtype>(width*height,
////                                   top_diff + width*height*dim_out
////                                   + width*height*N_*batch_img,
////                                   weight_data + width*height*dim_in
////                                   + width*height*K_*dim_out*batch_img,
////                                   weight_times_top_diff);

////                  caffe_add<Dtype>(width*height,
////                                   weight_times_top_diff,
////                                   bottom_data_mutable + width*height*dim_in
////                                   +width*height*K_*batch_img,
////                                   bottom_data_mutable + width*height*dim_in
////                                   +width*height*K_*batch_img);
////              }
////          }

////      }

//      const Dtype* weight_data_cpu = this->blobs_[0]->cpu_data();


//      /// We probabaly need to add another loop for summation..

//      for(int batch_img = 0; batch_img < M_; batch_img++ )
//      {
//          for(int yy = 0; yy < height ; yy++ )
//          {
//              for(int xx = 0; xx < width ; xx++ )
//              {
//                  for(int dim_in = 0; dim_in < Inchannels; dim_in++)
//                  {
//                      Dtype sum_val =0;

//                      for(int dim_out = 0; dim_out < Outchannels; dim_out++)
//                      {
//                          int idx = batch_img * width * height * Outchannels + yy * width + xx + width * height * dim_out;

//                          sum_val += top_diff[idx]*weight_data_cpu[dim_out*Inchannels + dim_in];
//                      }

//                      int idx_out = batch_img * width * height * Inchannels +  yy * width + xx + width * height * dim_in;

//                      bottom_data_mutable[idx_out] = sum_val;
//                  }
//              }
//          }

//      }

//  }

//}

template <typename Dtype>
Dtype InnerProductLayer<Dtype>::Forward_gpu(const vector<Blob<Dtype>*>& bottom,
    vector<Blob<Dtype>*>* top)
{

  return Forward_gpu_dense(bottom,top);

  const Dtype* bottom_data = bottom[0]->gpu_data();

  Dtype* top_data = (*top)[0]->mutable_gpu_data();

  const Dtype* weight = this->blobs_[0]->gpu_data();

  caffe_gpu_gemm<Dtype>(CblasNoTrans, CblasTrans, M_, N_, K_, (Dtype)1.,
      bottom_data, weight, (Dtype)0., top_data);

  if (bias_term_)
  {
    caffe_gpu_gemm<Dtype>(CblasNoTrans, CblasNoTrans, M_, N_, 1, (Dtype)1.,
        reinterpret_cast<const Dtype*>(bias_multiplier_->gpu_data()),
        this->blobs_[1]->gpu_data(), (Dtype)1., top_data);
  }

  return Dtype(0);
}

template <typename Dtype>
void InnerProductLayer<Dtype>::Backward_gpu(const vector<Blob<Dtype>*>& top,
    const vector<bool>& propagate_down,
    vector<Blob<Dtype>*>* bottom)
{

  return Backward_gpu_dense(top,propagate_down,bottom);

  const Dtype* top_diff = top[0]->gpu_diff();
  const Dtype* bottom_data = (*bottom)[0]->gpu_data();

  // Gradient with respect to weight
  caffe_gpu_gemm<Dtype>(CblasTrans, CblasNoTrans, N_, K_, M_, (Dtype)1.,
      top_diff, bottom_data, (Dtype)0., this->blobs_[0]->mutable_gpu_diff());
  
  if (bias_term_)
  {
    // Gradient with respect to bias
    caffe_gpu_gemv<Dtype>(CblasTrans, M_, N_, (Dtype)1., top_diff,
        reinterpret_cast<const Dtype*>(bias_multiplier_->gpu_data()),
        (Dtype)0., this->blobs_[1]->mutable_gpu_diff());
  }
  
  if (propagate_down[0])
  {
    // Gradient with respect to bottom data
    caffe_gpu_gemm<Dtype>(CblasNoTrans, CblasNoTrans, M_, K_, N_, (Dtype)1.,
        top_diff, this->blobs_[0]->gpu_data(), (Dtype)0.,
        (*bottom)[0]->mutable_gpu_diff());
  }
}

INSTANTIATE_CLASS(InnerProductLayer);

}  // namespace caffe

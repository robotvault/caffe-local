// Copyright 2014 Ankur Handa.

#include <stdint.h>
#include <leveldb/db.h>
#include <pthread.h>

#include <string>
#include <vector>

#include "caffe/layer.hpp"
#include "caffe/util/io.hpp"
#include "caffe/util/math_functions.hpp"
#include "caffe/util/rng.hpp"
#include "caffe/vision_layers.hpp"
#include "caffe/proto/caffe.pb.h"

#include "caffe/data_layers.hpp"

#include <iostream>

using std::string;


///New : CIFAR data layout - http://www.cs.toronto.edu/~kriz/cifar.html
///      First 1024 are R, next G, and remaining B.


namespace caffe {


///New

}

// Copyright 2014 BVLC and contributors.

#include <cmath>
#include <cstring>
#include <vector>

#include "cuda_runtime.h"
#include "gtest/gtest.h"
#include "caffe/blob.hpp"
#include "caffe/common.hpp"
#include "caffe/filler.hpp"
#include "caffe/vision_layers.hpp"
#include "caffe/test/test_gradient_check_util.hpp"

#include "caffe/test/test_caffe_main.hpp"

namespace caffe {

extern cudaDeviceProp CAFFE_TEST_CUDA_PROP;

template <typename Dtype>
class SoftmaxLayerTest : public ::testing::Test {
 protected:
  SoftmaxLayerTest()
        /// First element is number of images
        /// Second element is the number of channels
        /// Third element is height
        /// Fourth element is width

      : blob_bottom_(new Blob<Dtype>(1, 2, 2, 2)),
        blob_top_(new Blob<Dtype>()) {
    // fill the values
    FillerParameter filler_param;
    GaussianFiller<Dtype> filler(filler_param);
    filler.Fill(this->blob_bottom_);
    blob_bottom_vec_.push_back(blob_bottom_);
    blob_top_vec_.push_back(blob_top_);
  }
  virtual ~SoftmaxLayerTest() { delete blob_bottom_; delete blob_top_; }
  Blob<Dtype>* const blob_bottom_;
  Blob<Dtype>* const blob_top_;
  vector<Blob<Dtype>*> blob_bottom_vec_;
  vector<Blob<Dtype>*> blob_top_vec_;
};

typedef ::testing::Types</*float, */double> Dtypes;
TYPED_TEST_CASE(SoftmaxLayerTest, Dtypes);

//TYPED_TEST(SoftmaxLayerTest, TestForwardCPU)
//{
//  LayerParameter layer_param;
//  Caffe::set_mode(Caffe::CPU);
//  SoftmaxLayer<TypeParam> layer(layer_param);

//  layer.SetUp(this->blob_bottom_vec_, &(this->blob_top_vec_));
//  layer.Forward(this->blob_bottom_vec_, &(this->blob_top_vec_));

//  EXPECT_EQ(this->blob_bottom_->num(), this->blob_top_->num());
//  EXPECT_EQ(this->blob_bottom_->channels(), this->blob_top_->channels());
//  EXPECT_EQ(this->blob_bottom_->height(), this->blob_top_->height());
//  EXPECT_EQ(this->blob_bottom_->width(), this->blob_top_->width());


////  for (int i = 0; i < this->blob_top_->num(); ++i)
////  {
////      std::cout<<"************** From Test SOFTMAX Image No: " << i << std::endl;

////      for (int h = 0; h < this->blob_top_->height(); ++h)
////      {
////          for (int w = 0; w < this->blob_top_->width(); ++w)
////          {
////              std::cout<<"[ "<<w<< ","<<h<<" ] = ";

////              for (int j = 0; j < this->blob_top_->channels(); ++j)
////              {
////                   std::cout<<" "<<this->blob_bottom_->data_at(i, j, h, w);
////              }
////              std::cout<<std::endl;
////          }
////      }
////  }


  /// Test sum
//  for (int i = 0; i < this->blob_top_->num(); ++i)
//  {
//      for (int h = 0; h < this->blob_top_->height(); ++h)
//      {
//          for (int w = 0; w < this->blob_top_->width(); ++w)
//          {
//              TypeParam sum = 0;
//              for (int j = 0; j < this->blob_top_->channels(); ++j)
//              {
//                  sum += this->blob_top_->data_at(i, j, h, w);
//              }
//              EXPECT_GE(sum, TypeParam(0.999));
//              EXPECT_LE(sum, TypeParam(1.001));
//          }
//      }
//  }


//  /// Test exact values
//  for (int i = 0; i < this->blob_bottom_->num(); ++i)
//  {
//      for (int h = 0; h < this->blob_bottom_->height(); ++h)
//      {
//          for (int w = 0; w < this->blob_bottom_->width(); ++w)
//          {
//              TypeParam scale = 0;
//              for (int j = 0; j < this->blob_bottom_->channels(); ++j)
//              {
//                  scale += exp(this->blob_bottom_->data_at(i, j, h, w));
//              }
//              for (int j = 0; j < this->blob_bottom_->channels(); ++j)
//              {
//                  EXPECT_GE(this->blob_top_->data_at(i, j, h, w) + 1e-4,
//                          exp(this->blob_bottom_->data_at(i, j, h, w)) / scale)
//                      << "debug: " << i << " " << j;
//                  EXPECT_LE(this->blob_top_->data_at(i, j, h, w) - 1e-4,
//                          exp(this->blob_bottom_->data_at(i, j, h, w)) / scale)
//                      << "debug: " << i << " " << j;
//              }
//          }
//      }
//  }

//}




//TYPED_TEST(SoftmaxLayerTest, TestForwardGPU)
//{
//  LayerParameter layer_param;
//  Caffe::set_mode(Caffe::GPU);
//  SoftmaxLayer<TypeParam> layer(layer_param);

//  layer.SetUp(this->blob_bottom_vec_, &(this->blob_top_vec_));

////  for (int i = 0; i < this->blob_top_->num(); ++i)
////  {
////      std::cout<<"************** Before Test SOFTMAX Image No: " << i << std::endl;

////      for (int h = 0; h < this->blob_top_->height(); ++h)
////      {
////          for (int w = 0; w < this->blob_top_->width(); ++w)
////          {
////              std::cout<<"[ "<<w<< ","<<h<<" ] = ";

////              for (int j = 0; j < this->blob_top_->channels(); ++j)
////              {
////                   std::cout<<" "<<this->blob_bottom_->data_at(i, j, h, w);
////              }
////              std::cout<<std::endl;
////          }
////      }
////  }

//  layer.Forward(this->blob_bottom_vec_, &(this->blob_top_vec_));

//  EXPECT_EQ(this->blob_bottom_->num(), this->blob_top_->num());
//  EXPECT_EQ(this->blob_bottom_->channels(), this->blob_top_->channels());
//  EXPECT_EQ(this->blob_bottom_->height(), this->blob_top_->height());
//  EXPECT_EQ(this->blob_bottom_->width(), this->blob_top_->width());


////  for (int i = 0; i < this->blob_top_->num(); ++i)
////  {
////      std::cout<<"************** After Test SOFTMAX Image No: " << i << std::endl;

////      for (int h = 0; h < this->blob_top_->height(); ++h)
////      {
////          for (int w = 0; w < this->blob_top_->width(); ++w)
////          {
////              std::cout<<"[ "<<w<< ","<<h<<" ] = ";

////              for (int j = 0; j < this->blob_top_->channels(); ++j)
////              {
////                   std::cout<<" "<<this->blob_top_->data_at(i, j, h, w);
////              }
////              std::cout<<std::endl;
////          }
////      }
////  }


//  /// Test sum
//  for (int i = 0; i < this->blob_top_->num(); ++i)
//  {
//      for (int h = 0; h < this->blob_top_->height(); ++h)
//      {
//          for (int w = 0; w < this->blob_top_->width(); ++w)
//          {
//              TypeParam sum = 0;
//              for (int j = 0; j < this->blob_top_->channels(); ++j)
//              {
//                  sum += this->blob_top_->data_at(i, j, h, w);
//              }
//              EXPECT_GE(sum, TypeParam(0.999));
//              EXPECT_LE(sum, TypeParam(1.001));
//          }
//      }
//  }


//  /// Test exact values
////  for (int i = 0; i < this->blob_bottom_->num(); ++i)
////  {
////      for (int h = 0; h < this->blob_bottom_->height(); ++h)
////      {
////          for (int w = 0; w < this->blob_bottom_->width(); ++w)
////          {
////              TypeParam scale = 0;
////              for (int j = 0; j < this->blob_bottom_->channels(); ++j)
////              {
////                  scale += exp(this->blob_bottom_->data_at(i, j, h, w));
////              }
////              for (int j = 0; j < this->blob_bottom_->channels(); ++j)
////              {
////                  EXPECT_GE(this->blob_top_->data_at(i, j, h, w) + 1e-4,
////                          exp(this->blob_bottom_->data_at(i, j, h, w)) / scale)
////                      << "debug: " << i << " " << j;
////                  EXPECT_LE(this->blob_top_->data_at(i, j, h, w) - 1e-4,
////                          exp(this->blob_bottom_->data_at(i, j, h, w)) / scale)
////                      << "debug: " << i << " " << j;
////              }
////          }
////      }
////  }

//}

////TYPED_TEST(SoftmaxLayerTest, TestGradientCPU)
////{
////  LayerParameter layer_param;
////  Caffe::set_mode(Caffe::CPU);

////  SoftmaxLayer<TypeParam> layer(layer_param);

////  GradientChecker<TypeParam> checker(1e-2, 1e-3);

////  checker.CheckGradientExhaustive(&layer, &(this->blob_bottom_vec_), &(this->blob_top_vec_));
////}

TYPED_TEST(SoftmaxLayerTest, TestGradientGPU)
{
  LayerParameter layer_param;
  Caffe::set_mode(Caffe::GPU);

  SoftmaxLayer<TypeParam> layer(layer_param);

  GradientChecker<TypeParam> checker(1e-2, 1e-3);

  checker.CheckGradientExhaustive(&layer, &(this->blob_bottom_vec_), &(this->blob_top_vec_));
}

}  // namespace caffe



//// Copyright 2014 BVLC and contributors.

//#include <cmath>
//#include <cstring>
//#include <vector>

//#include "cuda_runtime.h"
//#include "gtest/gtest.h"
//#include "caffe/blob.hpp"
//#include "caffe/common.hpp"
//#include "caffe/filler.hpp"
//#include "caffe/vision_layers.hpp"
//#include "caffe/test/test_gradient_check_util.hpp"

//#include "caffe/test/test_caffe_main.hpp"

//namespace caffe {

//extern cudaDeviceProp CAFFE_TEST_CUDA_PROP;

//template <typename Dtype>
//class SoftmaxLayerTest : public ::testing::Test {
// protected:
//  SoftmaxLayerTest()
//      : blob_bottom_(new Blob<Dtype>(2, 10, 1, 1)),
//        blob_top_(new Blob<Dtype>()) {
//    // fill the values
//    FillerParameter filler_param;
//    GaussianFiller<Dtype> filler(filler_param);
//    filler.Fill(this->blob_bottom_);
//    blob_bottom_vec_.push_back(blob_bottom_);
//    blob_top_vec_.push_back(blob_top_);
//  }
//  virtual ~SoftmaxLayerTest() { delete blob_bottom_; delete blob_top_; }
//  Blob<Dtype>* const blob_bottom_;
//  Blob<Dtype>* const blob_top_;
//  vector<Blob<Dtype>*> blob_bottom_vec_;
//  vector<Blob<Dtype>*> blob_top_vec_;
//};

//typedef ::testing::Types<float, double> Dtypes;
//TYPED_TEST_CASE(SoftmaxLayerTest, Dtypes);

//TYPED_TEST(SoftmaxLayerTest, TestForwardCPU) {
//  LayerParameter layer_param;
//  Caffe::set_mode(Caffe::CPU);
//  SoftmaxLayer<TypeParam> layer(layer_param);
//  layer.SetUp(this->blob_bottom_vec_, &(this->blob_top_vec_));
//  layer.Forward(this->blob_bottom_vec_, &(this->blob_top_vec_));
//  // Test sum
//  for (int i = 0; i < this->blob_bottom_->num(); ++i) {
//    TypeParam sum = 0;
//    for (int j = 0; j < this->blob_top_->channels(); ++j) {
//      sum += this->blob_top_->data_at(i, j, 0, 0);
//    }
//    EXPECT_GE(sum, 0.999);
//    EXPECT_LE(sum, 1.001);
//  }
//  // Test exact values
//  for (int i = 0; i < this->blob_bottom_->num(); ++i) {
//    TypeParam scale = 0;
//    for (int j = 0; j < this->blob_bottom_->channels(); ++j) {
//      scale += exp(this->blob_bottom_->data_at(i, j, 0, 0));
//    }
//    for (int j = 0; j < this->blob_bottom_->channels(); ++j) {
//      EXPECT_GE(this->blob_top_->data_at(i, j, 0, 0) + 1e-4,
//          exp(this->blob_bottom_->data_at(i, j, 0, 0)) / scale)
//          << "debug: " << i << " " << j;
//      EXPECT_LE(this->blob_top_->data_at(i, j, 0, 0) - 1e-4,
//          exp(this->blob_bottom_->data_at(i, j, 0, 0)) / scale)
//          << "debug: " << i << " " << j;
//    }
//  }
//}

//TYPED_TEST(SoftmaxLayerTest, TestGradientCPU) {
//  LayerParameter layer_param;
//  Caffe::set_mode(Caffe::CPU);
//  SoftmaxLayer<TypeParam> layer(layer_param);
//  GradientChecker<TypeParam> checker(1e-2, 1e-3);
//  checker.CheckGradientExhaustive(&layer, &(this->blob_bottom_vec_),
//      &(this->blob_top_vec_));
//}

//}  // namespace caffe

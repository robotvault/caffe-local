// Copyright 2014 BVLC and contributors.

#include <cstring>
#include <vector>

#include "cuda_runtime.h"
#include "gtest/gtest.h"
#include "caffe/blob.hpp"
#include "caffe/common.hpp"
#include "caffe/filler.hpp"
#include "caffe/vision_layers.hpp"
#include "caffe/test/test_gradient_check_util.hpp"

#include "caffe/test/test_caffe_main.hpp"

namespace caffe {

extern cudaDeviceProp CAFFE_TEST_CUDA_PROP;

template <typename Dtype>
class InnerProductLayerTest : public ::testing::Test {
 protected:
  InnerProductLayerTest()
      : blob_bottom_(new Blob<Dtype>(80, 23, 5, 2)),
        blob_top_(new Blob<Dtype>())
  {
    /// fill the values
    FillerParameter filler_param;

    UniformFiller<Dtype> filler(filler_param);

    filler.Fill(this->blob_bottom_);

    blob_bottom_vec_.push_back(blob_bottom_);

    blob_top_vec_.push_back(blob_top_);
  }

  virtual ~InnerProductLayerTest() { delete blob_bottom_; delete blob_top_; }
  Blob<Dtype>* const blob_bottom_;
  Blob<Dtype>* const blob_top_;
  vector<Blob<Dtype>*> blob_bottom_vec_;
  vector<Blob<Dtype>*> blob_top_vec_;
};

typedef ::testing::Types</*float,*/ double> Dtypes;
TYPED_TEST_CASE(InnerProductLayerTest, Dtypes);

//TYPED_TEST(InnerProductLayerTest, TestSetUp)
//{
//  LayerParameter layer_param;

//  int n_outputs = 10;

//  InnerProductParameter* inner_product_param = layer_param.mutable_inner_product_param();

//  inner_product_param->set_num_output(n_outputs);

//  shared_ptr<InnerProductLayer<TypeParam> > layer(new InnerProductLayer<TypeParam>(layer_param));

//  layer->SetUp(this->blob_bottom_vec_, &(this->blob_top_vec_));

//  EXPECT_EQ(this->blob_top_->num(), this->blob_bottom_->num());
//  EXPECT_EQ(this->blob_top_->height(), this->blob_bottom_->height());
//  EXPECT_EQ(this->blob_top_->width(), this->blob_bottom_->width());
//  EXPECT_EQ(this->blob_top_->channels(), n_outputs);
//}

//TYPED_TEST(InnerProductLayerTest, TestCPU)
//{
//  LayerParameter layer_param;
//  InnerProductParameter* inner_product_param = layer_param.mutable_inner_product_param();

//  Caffe::set_mode(Caffe::CPU);

//  int n_outputs = 10;


//  inner_product_param->set_num_output(n_outputs);
////  inner_product_param->mutable_weight_filler()->set_type("gaussian");
////  inner_product_param->mutable_bias_filler()->set_type("gaussian");
//  inner_product_param->mutable_weight_filler()->set_type("uniform");
//  inner_product_param->mutable_bias_filler()->set_type("uniform");
//  inner_product_param->mutable_bias_filler()->set_min(1);
//  inner_product_param->mutable_bias_filler()->set_max(2);

//  shared_ptr<InnerProductLayer<TypeParam> > layer(new InnerProductLayer<TypeParam>(layer_param));

//  layer->SetUp(this->blob_bottom_vec_, &(this->blob_top_vec_));
//  layer->Forward(this->blob_bottom_vec_, &(this->blob_top_vec_));


////  layer->blobs_[0];

//  for(int batch_img = 0; batch_img < this->blob_bottom_->num(); batch_img++)
//  {
//      for(int yy = 0; yy < this->blob_bottom_->height();yy++)
//      {
//          for(int xx = 0; xx < this->blob_bottom_->width(); xx++ )
//          {
//              for(int d = 0; d < this->blob_bottom_->channels(); d++)
//              {
//                   std::cout<<" "<<this->blob_bottom_->data_at(batch_img, d, yy, xx);
//              }

//              std::cout<<std::endl;
//          }
//      }
//  }

////  for(int yy = 0; yy < this->blob_bottom_->height();yy++)
////  {
////      for(int xx = 0; xx < this->blob_bottom_->width(); xx++ )
////      {
////          std::cout<<" "<<inner_product_param->mutable_weight  ->data_at(batch_img, d, yy, xx);
////      }
////  }


////  inner_product_param->weight_filler();

//  const TypeParam* data = this->blob_top_->cpu_data();
//  const int count = this->blob_top_->count();

//  for (int i = 0; i < count; ++i)
//  {
//     std::cout<<" " << data[i];
////     EXPECT_LE(data[i], 1.);
//  }
//  std::cout<<std::endl;


//  for (int i = 0; i < count; ++i)
//  {
////     std::cout<<" " << data[i];
//     EXPECT_GE(data[i], 1.);
//  }
//  std::cout<<std::endl;

//}

//TYPED_TEST(InnerProductLayerTest, TestForwardGPU)
//{
//  //if (sizeof(TypeParam) == 4 || CAFFE_TEST_CUDA_PROP.major >= 2)
//  {
//    LayerParameter layer_param;
//    InnerProductParameter* inner_product_param =
//        layer_param.mutable_inner_product_param();

//    Caffe::set_mode(Caffe::GPU);

//    int n_outputs = 10;

//    inner_product_param->set_num_output(n_outputs);
//    inner_product_param->mutable_weight_filler()->set_type("uniform");
//    inner_product_param->mutable_bias_filler()->set_type("uniform");
//    inner_product_param->mutable_bias_filler()->set_min(1);
//    inner_product_param->mutable_bias_filler()->set_max(2);

//    shared_ptr<InnerProductLayer<TypeParam> > layer(
//      new InnerProductLayer<TypeParam>(layer_param));

//    layer->SetUp(this->blob_bottom_vec_, &(this->blob_top_vec_));
//    layer->Forward(this->blob_bottom_vec_, &(this->blob_top_vec_));

//    for(int batch_img = 0; batch_img < this->blob_bottom_->num(); batch_img++)
//    {
//        for(int yy = 0; yy < this->blob_bottom_->height();yy++)
//        {
//            for(int xx = 0; xx < this->blob_bottom_->width(); xx++ )
//            {
//                for(int d = 0; d < this->blob_bottom_->channels(); d++)
//                {
//                    std::cout<<" "<<this->blob_bottom_->data_at(batch_img, d, yy, xx);
//                }

//                std::cout<<std::endl;
//            }
//        }
//    }


//    const TypeParam* data = this->blob_top_->cpu_data();
//    const int count = this->blob_top_->count();

//    std::cout<<"Printing Data" << std::endl;

////    for (int i = 0; i < count; ++i)
////    {
////       std::cout<<" count"<<data[i];
////      //EXPECT_GE(data[i], 1.);
////    }

//    int __height = this->blob_top_->height();
//    int __width  = this->blob_top_->width();
//    int __dim    = this->blob_top_->channels();

//    std::cout<<"height = " << __height<<", width = " << __width<<", __channels = " << __dim<< std::endl;

//    for(int batch_imgs = 0; batch_imgs < this->blob_top_->num(); batch_imgs++)
//    {
//        for (int yy = 0; yy < __height; yy++ )
//        {
//            for (int xx = 0; xx < __width; xx++ )
//            {
//                for(int d = 0; d < __dim; d++)
//                {
//                    std::cout<<" "<<data[yy*__width+xx + d*__width*__height + batch_imgs*__width*__height*__dim];
//                }
//                std::cout<<"**"<<std::endl;
//            }
//            //EXPECT_GE(data[i], 1.);
//        }

//    }


//    std::cout<<std::endl;

//    for (int i = 0; i < count; ++i)
//    {
//      EXPECT_GE(data[i], 1.);
//    }
//  }

////    else
////    {
////        LOG(ERROR) << "Skipping test due to old architecture.";
////    }

//}

//TYPED_TEST(InnerProductLayerTest, TestCPUGradient)
//{
//  LayerParameter layer_param;

//  InnerProductParameter* inner_product_param = layer_param.mutable_inner_product_param();

//  Caffe::set_mode(Caffe::CPU);

//  inner_product_param->set_num_output(2);
//  inner_product_param->mutable_weight_filler()->set_type("gaussian");
//  inner_product_param->mutable_bias_filler()->set_type("gaussian");
//  inner_product_param->mutable_bias_filler()->set_min(1);
//  inner_product_param->mutable_bias_filler()->set_max(2);

//  InnerProductLayer<TypeParam> layer(layer_param);

//  GradientChecker<TypeParam> checker(1e-2, 1e-3);

//  checker.CheckGradientExhaustive(&layer, &(this->blob_bottom_vec_),
//      &(this->blob_top_vec_));
//}

TYPED_TEST(InnerProductLayerTest, TestGPUGradient) {
//  if (sizeof(TypeParam) == 4 || CAFFE_TEST_CUDA_PROP.major >= 2)


    LayerParameter layer_param;
    InnerProductParameter* inner_product_param = layer_param.mutable_inner_product_param();

    Caffe::set_mode(Caffe::GPU);

    inner_product_param->set_num_output(15);
    inner_product_param->mutable_weight_filler()->set_type("gaussian");
    inner_product_param->mutable_bias_filler()->set_type("gaussian");

    InnerProductLayer<TypeParam> layer(layer_param);

    GradientChecker<TypeParam> checker(1e-2, 1e-3);

    checker.CheckGradient(&layer, &(this->blob_bottom_vec_), &(this->blob_top_vec_));

//    checker.CheckGradientExhaustive(&layer, &(this->blob_bottom_vec_), &(this->blob_top_vec_));

//    else {
//        LOG(ERROR) << "Skipping test due to old architecture.";
//    }
}

}  // namespace caffe

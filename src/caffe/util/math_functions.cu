// Copyright 2014 BVLC and contributors.

#include <math_functions.h>  // CUDA's, not caffe's, for fabs, signbit
#include <thrust/device_vector.h>
#include <thrust/functional.h>  // thrust::plus
#include <thrust/reduce.h>
#include <cmath>
#include <cstdlib>
#include <cstring>

#include "caffe/common.hpp"
#include "caffe/util/math_functions.hpp"

#include <boost/math/common_factor_rt.hpp>

namespace caffe {

template <typename Dtype>
__global__ void set_kernel(const int n, const Dtype alpha, Dtype* y) {
  CUDA_KERNEL_LOOP(index, n) {
    y[index] = alpha;
  }
}

template <typename Dtype>
void caffe_gpu_set(const int N, const Dtype alpha, Dtype* Y) {
  if (alpha == 0) {
    CUDA_CHECK(cudaMemset(Y, 0, sizeof(Dtype) * N));
    return;
  }
  // NOLINT_NEXT_LINE(whitespace/operators)
  set_kernel<Dtype><<<CAFFE_GET_BLOCKS(N), CAFFE_CUDA_NUM_THREADS>>>(
      N, alpha, Y);
}

template void caffe_gpu_set<int>(const int N, const int alpha, int* Y);
template void caffe_gpu_set<float>(const int N, const float alpha, float* Y);
template void caffe_gpu_set<double>(const int N, const double alpha, double* Y);

template <typename Dtype>
__global__ void add_scalar_kernel(const int n, const Dtype alpha, Dtype* y) {
  CUDA_KERNEL_LOOP(index, n) {
    y[index] += alpha;
  }
}

template <>
void caffe_gpu_add_scalar(const int N, const float alpha, float* Y) {
  // NOLINT_NEXT_LINE(whitespace/operators)
  add_scalar_kernel<float><<<CAFFE_GET_BLOCKS(N), CAFFE_CUDA_NUM_THREADS>>>(
      N, alpha, Y);
}

template <>
void caffe_gpu_add_scalar(const int N, const double alpha, double* Y) {
  // NOLINT_NEXT_LINE(whitespace/operators)
  add_scalar_kernel<double><<<CAFFE_GET_BLOCKS(N), CAFFE_CUDA_NUM_THREADS>>>(
      N, alpha, Y);
}

template <typename Dtype>
__global__ void add_kernel(const int n, const Dtype* a,
    const Dtype* b, Dtype* y) {
  CUDA_KERNEL_LOOP(index, n) {
    y[index] = a[index] + b[index];
  }
}

template <>
void caffe_gpu_add<float>(const int N, const float* a, const float* b,
    float* y) {
  // NOLINT_NEXT_LINE(whitespace/operators)
  add_kernel<float><<<CAFFE_GET_BLOCKS(N), CAFFE_CUDA_NUM_THREADS>>>(
      N, a, b, y);
}

template <>
void caffe_gpu_add<double>(const int N, const double* a, const double* b,
    double* y) {
  // NOLINT_NEXT_LINE(whitespace/operators)
  add_kernel<double><<<CAFFE_GET_BLOCKS(N), CAFFE_CUDA_NUM_THREADS>>>(
      N, a, b, y);
}

template <typename Dtype>
__global__ void sub_kernel(const int n, const Dtype* a,
    const Dtype* b, Dtype* y) {
  CUDA_KERNEL_LOOP(index, n) {
    y[index] = a[index] - b[index];
  }
}

template <>
void caffe_gpu_sub<float>(const int N, const float* a, const float* b,
    float* y) {
  // NOLINT_NEXT_LINE(whitespace/operators)
  sub_kernel<float><<<CAFFE_GET_BLOCKS(N), CAFFE_CUDA_NUM_THREADS>>>(
      N, a, b, y);
}

template <>
void caffe_gpu_sub<double>(const int N, const double* a, const double* b,
    double* y) {
  // NOLINT_NEXT_LINE(whitespace/operators)
  sub_kernel<double><<<CAFFE_GET_BLOCKS(N), CAFFE_CUDA_NUM_THREADS>>>(
      N, a, b, y);
}

template <typename Dtype>
__global__ void mul_kernel(const int n, const Dtype* a,
    const Dtype* b, Dtype* y) {
  CUDA_KERNEL_LOOP(index, n) {
    y[index] = a[index] * b[index];
  }
}

template <>
void caffe_gpu_mul<float>(const int N, const float* a,
    const float* b, float* y) {
  // NOLINT_NEXT_LINE(whitespace/operators)
  mul_kernel<float><<<CAFFE_GET_BLOCKS(N), CAFFE_CUDA_NUM_THREADS>>>(
      N, a, b, y);
}

template <>
void caffe_gpu_mul<double>(const int N, const double* a,
    const double* b, double* y) {
  // NOLINT_NEXT_LINE(whitespace/operators)
  mul_kernel<double><<<CAFFE_GET_BLOCKS(N), CAFFE_CUDA_NUM_THREADS>>>(
      N, a, b, y);
}

template <typename Dtype>
__global__ void div_kernel(const int n, const Dtype* a,
    const Dtype* b, Dtype* y) {
  CUDA_KERNEL_LOOP(index, n) {
    y[index] = a[index] / b[index];
  }
}

template <>
void caffe_gpu_div<float>(const int N, const float* a,
    const float* b, float* y) {
  // NOLINT_NEXT_LINE(whitespace/operators)
  div_kernel<float><<<CAFFE_GET_BLOCKS(N), CAFFE_CUDA_NUM_THREADS>>>(
      N, a, b, y);
}

template <>
void caffe_gpu_div<double>(const int N, const double* a,
    const double* b, double* y) {
  // NOLINT_NEXT_LINE(whitespace/operators)
  div_kernel<double><<<CAFFE_GET_BLOCKS(N), CAFFE_CUDA_NUM_THREADS>>>(
      N, a, b, y);
}

template <typename Dtype>
__global__ void powx_kernel(const int n, const Dtype* a,
    const Dtype alpha, Dtype* y) {
  CUDA_KERNEL_LOOP(index, n) {
    y[index] = pow(a[index], alpha);
  }
}

template <typename Dtype>

__global__ void AX_kernel(const Dtype* w,
                             const int w_width, const int w_height,
                             const Dtype* X,
                             const int x_width, const int x_height,
                             Dtype* y,
                             const int img_no)
{
    int OutChannels = w_height;
    int InChannels  = w_width;

    uint xx = blockIdx.x*blockDim.x + threadIdx.x;
    uint yy = blockIdx.y*blockDim.y + threadIdx.y;


    for(int dim_in = 0; dim_in < InChannels; dim_in++)
    {
        Dtype w_b_row = (Dtype)(0.0);

        for(int dim_out = 0; dim_out < OutChannels; dim_out++ )
        {
            int idx_in =  x_width * x_height * dim_out + yy * x_width + xx;

            w_b_row += X[idx_in] * w[dim_in + dim_out * InChannels];

        }

        int idx_out = x_width * x_height * dim_in + yy * x_width + xx;

        y[idx_out] = w_b_row;
    }
}





template<>
void caffe_gpu_AX<float>(const float* w,
                         const int w_width, const int w_height,
                         const float* X,
                         const int x_width, const int x_height,
                         float* y,
                         const int img_no)
{

    const int blockWidthx = 32;
    const int blockWidthy = 32;

    const dim3 dimBlock(boost::math::gcd<unsigned>( x_width , blockWidthx),
                        boost::math::gcd<unsigned>(x_height , blockWidthy),
                        1);

    const dim3 dimGrid( x_width / dimBlock.x,
                       x_height / dimBlock.y,
                       1);


    AX_kernel<float><<<dimGrid,dimBlock>>>(w,w_width,
                                           w_height,
                                           X,
                                           x_width,
                                           x_height,
                                           y,
                                           img_no);
}


template<>
void caffe_gpu_AX<double>(const double* w,
                          const int w_width, const int w_height,
                          const double* X,
                          const int x_width, const int x_height,
                          double* y,
                          const int img_no)
{


    const int blockWidthx = 32;
    const int blockWidthy = 32;

    const dim3 dimBlock(boost::math::gcd<unsigned>( x_width , blockWidthx),
                        boost::math::gcd<unsigned>(x_height , blockWidthy),
                        1);

    const dim3 dimGrid( x_width / dimBlock.x,
                       x_height / dimBlock.y,
                       1);

    AX_kernel<double><<<dimGrid,dimBlock>>>(w,w_width,
                                            w_height,
                                            X,
                                            x_width,
                                            x_height,
                                            y,
                                            img_no);
}

template <typename Dtype>

__global__ void AXpB_kernel( const Dtype* w,
                             const int w_width, const int w_height,
                             const Dtype* b,
                             const Dtype* X,
                             const int x_width, const int x_height,
                             Dtype* y,
                             const int img_no)
{
    int OutChannels = w_height;
    int InChannels  = w_width;

    uint xx = blockIdx.x*blockDim.x + threadIdx.x;
    uint yy = blockIdx.y*blockDim.y + threadIdx.y;

    if ( yy < x_height && xx < x_width )
    {
        for(int dim_out = 0; dim_out < OutChannels; dim_out++ )
        {
            Dtype w_b_row = (Dtype)(0.0);

            for(int dim_in = 0; dim_in < InChannels; dim_in++ )
            {
                int idx_in =  /*img_no * x_width * x_height * InChannels + */x_width * x_height * dim_in +
                        yy * x_width + xx;

                w_b_row += X[idx_in] * w[dim_in + dim_out * InChannels];

            }

            int idx_out = /*img_no * x_width * x_height * OutChannels +*/ x_width * x_height * dim_out + yy * x_width + xx;

            y[idx_out] = w_b_row + b[dim_out];
        }
    }


}

template<>
void caffe_gpu_AXpB<float>(const float* w,
                           const int w_width, const int w_height,
                           const float* b,
                           const float* X,
                           const int x_width, const int x_height,
                           float* y,
                           const int img_no)
{


    const int blockWidthx = 32;
    const int blockWidthy = 32;

    const dim3 dimBlock(boost::math::gcd<unsigned>( x_width , blockWidthx),
                        boost::math::gcd<unsigned>(x_height , blockWidthy),
                        1);

    const dim3 dimGrid( x_width / dimBlock.x,
                       x_height / dimBlock.y,
                       1);



    AXpB_kernel<float><<<dimGrid, dimBlock>>>(w,w_width,
                                              w_height,
                                              b,
                                              X,
                                              x_width,
                                              x_height,
                                              y,
                                              img_no);
}


template<>
void caffe_gpu_AXpB<double>(const double* w,
                            const int w_width, const int w_height,
                            const double* b,
                            const double* X,
                            const int x_width, const int x_height,
                            double* y,
                            const int img_no)
{
    const int blockWidthx = 32;
    const int blockWidthy = 32;

    const dim3 dimBlock(boost::math::gcd<unsigned>( x_width , blockWidthx),
                        boost::math::gcd<unsigned>(x_height , blockWidthy),
                        1);

    const dim3 dimGrid( x_width / dimBlock.x,
                       x_height / dimBlock.y,
                       1);


    AXpB_kernel<double><<<dimGrid, dimBlock>>>(w,w_width,
                                               w_height,
                                               b,
                                               X,
                                               x_width,
                                               x_height,
                                               y,
                                               img_no);
}



template <typename Dtype>
__global__ void gpu_exp_axpby_dense_kernel(const int width,
                                       const int height,
                                       const int dim,
                                       const Dtype alpha,
                                       const Dtype* X,
                                       const Dtype beta,
                                       Dtype* Y)
{
    uint xx = blockIdx.x*blockDim.x + threadIdx.x;
    uint yy = blockIdx.y*blockDim.y + threadIdx.y;


    if ( xx < width && yy < height )
    {
//        Dtype sum = 0;

#pragma unroll
        for(int i = 0; i < dim; i++)
        {
            int ind_y = i*width*height + yy*width + xx;
            int ind_x = yy*width+xx;

            Y[ind_y] = __expf(beta*Y[ind_y] + alpha* X[ind_x]);

        }

    }

}


template <typename Dtype>
__global__ void gpu_axpby_dense_kernel(const int width,
                                       const int height,
                                       const int dim,
                                       const Dtype alpha,
                                       const Dtype* X,
                                       const Dtype beta,
                                       Dtype* Y)
{
    uint xx = blockIdx.x*blockDim.x + threadIdx.x;
    uint yy = blockIdx.y*blockDim.y + threadIdx.y;


    if ( xx < width && yy < height )
    {
//        Dtype sum = 0;

#pragma unroll
        for(int i = 0; i < dim; i++)
        {
            int ind_y = i*width*height + yy*width + xx;
            int ind_x = yy*width+xx;

            Y[ind_y] =  beta*Y[ind_y] + alpha* X[ind_x];

        }

    }

}



template<>
void caffe_gpu_axpby_dense<float>(const int width,
                                  const int height,
                                  const int dim,
                                  const float alpha,
                                  const float * X,
                                  const float beta,
                                  float* Y)
{


    const int blockWidthx = 32;
    const int blockWidthy = 32;

    const dim3 dimBlock(boost::math::gcd<unsigned>( width , blockWidthx),
                        boost::math::gcd<unsigned>(height , blockWidthy),
                        1);

    const dim3 dimGrid( width / dimBlock.x,
                       height / dimBlock.y,
                       1);



    gpu_axpby_dense_kernel<float><<<dimGrid, dimBlock>>>(width,
                                                                                 height,
                                                                                 dim,
                                                                                 alpha,
                                                                                 X,
                                                                                 beta,
                                                                                 Y);
}


template<>
void caffe_gpu_axpby_dense<double>(const int width,
                                  const int height,
                                  const int dim,
                                  const double alpha,
                                  const double* X,
                                  const double beta,
                                  double* Y)
{
    const int blockWidthx = 32;
    const int blockWidthy = 32;

    const dim3 dimBlock(boost::math::gcd<unsigned>( width , blockWidthx),
                        boost::math::gcd<unsigned>(height , blockWidthy),
                        1);

    const dim3 dimGrid( width / dimBlock.x,
                       height / dimBlock.y,
                       1);


    gpu_axpby_dense_kernel<double><<<dimGrid, dimBlock>>>(width,
                                                                                 height,
                                                                                 dim,
                                                                                 alpha,
                                                                                 X,
                                                                                 beta,
                                                                                 Y);
}



template<>
void caffe_gpu_exp_axpby_dense<float>(const int width,
                                  const int height,
                                  const int dim,
                                  const float alpha,
                                  const float* X,
                                  const float beta,
                                  float* Y)
{
    const int blockWidthx = 32;
    const int blockWidthy = 32;

    const dim3 dimBlock(boost::math::gcd<unsigned>( width , blockWidthx),
                        boost::math::gcd<unsigned>(height , blockWidthy),
                        1);

    const dim3 dimGrid( width / dimBlock.x,
                       height / dimBlock.y,
                       1);


    gpu_exp_axpby_dense_kernel<float><<<dimGrid, dimBlock>>>(width,
                                                         height,
                                                         dim,
                                                         alpha,
                                                         X,
                                                         beta,
                                                         Y);
}



template<>
void caffe_gpu_exp_axpby_dense<double>(const int width,
                                  const int height,
                                  const int dim,
                                  const double alpha,
                                  const double* X,
                                  const double beta,
                                  double* Y)
{
    const int blockWidthx = 32;
    const int blockWidthy = 32;

    const dim3 dimBlock(boost::math::gcd<unsigned>( width , blockWidthx),
                        boost::math::gcd<unsigned>(height , blockWidthy),
                        1);

    const dim3 dimGrid( width / dimBlock.x,
                       height / dimBlock.y,
                       1);


    gpu_exp_axpby_dense_kernel<double><<<dimGrid, dimBlock>>>(width,
                                                                                 height,
                                                                                 dim,
                                                                                 alpha,
                                                                                 X,
                                                                                 beta,
                                                                                 Y);
}




template <typename Dtype>

__global__ void gpu_sum_and_div_dense_kernel(const int width,
                                             const int height,
                                             const int dim,
                                             Dtype* X,
                                             Dtype* Y)
{
    uint xx = blockIdx.x*blockDim.x + threadIdx.x;
    uint yy = blockIdx.y*blockDim.y + threadIdx.y;

    if ( xx < width && yy < height )
    {
        Dtype sum = 0;


#pragma unroll

        for(int i = 0; i < dim; i++)
        {
            int ind_y = i*width*height + yy*width + xx;
             sum += Y[ind_y];
        }

#pragma unroll

        for(int i = 0; i < dim; i++)
        {
            int ind_y = i*width*height + yy*width + xx;
            Y[ind_y] = Y[ind_y] / sum;
        }
    }

}


template<>
void caffe_gpu_sum_and_div_dense<float>(const int width,
                                        const int height,
                                        const int dim,
                                        float* X,
                                        float* Y)
{
    const int blockWidthx = 16;
    const int blockWidthy = 16;

    const dim3 dimBlock(boost::math::gcd<unsigned>( width , blockWidthx),
                        boost::math::gcd<unsigned>(height , blockWidthy),
                        1);

    const dim3 dimGrid( width / dimBlock.x,
                       height / dimBlock.y,
                       1);


    gpu_sum_and_div_dense_kernel<float><<<dimGrid, dimBlock>>>(width,
                                                               height,
                                                               dim,
                                                               X,
                                                               Y);
}


template<>
void caffe_gpu_sum_and_div_dense<double>(const int width,
                                         const int height,
                                         const int dim,
                                         double* X,
                                         double* Y)
{

    const int blockWidthx = 32;
    const int blockWidthy = 32;

    const dim3 dimBlock(boost::math::gcd<unsigned>( width , blockWidthx),
                        boost::math::gcd<unsigned>(height , blockWidthy),
                        1);

    const dim3 dimGrid( width / dimBlock.x,
                       height / dimBlock.y,
                       1);

    gpu_sum_and_div_dense_kernel<double><<<dimGrid, dimBlock>>>(width,
                                                                height,
                                                                dim,
                                                                X,
                                                                Y);
}

template <typename Dtype>

__global__ void gpu_mul_and_add_dense_kernel(const int width,
                                             const int height,
                                             const int dim,
                                             const Dtype* a,
                                             const Dtype* b,
                                             Dtype* y)
{
    uint xx = blockIdx.x*blockDim.x + threadIdx.x;
    uint yy = blockIdx.y*blockDim.y + threadIdx.y;

    if ( xx < width && yy < height )
    {
        Dtype sum = 0;

        int ind_out = yy*width + xx;

//#pragma unroll

        for(int i = 0; i < dim; i++)
        {
            int ind_in = i*width*height + yy*width + xx;

            sum += a[ind_in]*b[ind_in];
        }

        y[ind_out] = sum;
    }

}


template <>
void caffe_gpu_mul_and_add_dense<float>(const int width,
                                        const int height,
                                        const int dim,
                                        const float* a,
                                        const float* b,
                                        float* y)
{
    const int blockWidthx = 32;
    const int blockWidthy = 32;

    const dim3 dimBlock(boost::math::gcd<unsigned>( width , blockWidthx),
                        boost::math::gcd<unsigned>(height , blockWidthy),
                        1);

    const dim3 dimGrid( width / dimBlock.x,
                       height / dimBlock.y,
                       1);


    gpu_mul_and_add_dense_kernel<float><<<dimGrid, dimBlock>>>(width,
                                                        height,
                                                        dim,
                                                        a,
                                                        b,
                                                        y);
}


template <>
void caffe_gpu_mul_and_add_dense<double>(const int width,
                                        const int height,
                                        const int dim,
                                        const double* a,
                                        const double* b,
                                        double* y)
{
    const int blockWidthx = 32;
    const int blockWidthy = 32;

    const dim3 dimBlock(boost::math::gcd<unsigned>( width , blockWidthx),
                        boost::math::gcd<unsigned>(height , blockWidthy),
                        1);

    const dim3 dimGrid( width / dimBlock.x,
                       height / dimBlock.y,
                       1);


    gpu_mul_and_add_dense_kernel<double><<<dimGrid, dimBlock>>>(width,
                                                        height,
                                                        dim,
                                                        a,
                                                        b,
                                                        y);
}


template <>
void caffe_gpu_powx<float>(const int N, const float* a,
    const float alpha, float* y) {
  // NOLINT_NEXT_LINE(whitespace/operators)
  powx_kernel<float><<<CAFFE_GET_BLOCKS(N), CAFFE_CUDA_NUM_THREADS>>>(
      N, a, alpha, y);
}

template <>
void caffe_gpu_powx<double>(const int N, const double* a,
    const double alpha, double* y) {
  // NOLINT_NEXT_LINE(whitespace/operators)
  powx_kernel<double><<<CAFFE_GET_BLOCKS(N), CAFFE_CUDA_NUM_THREADS>>>(
      N, a, alpha, y);
}

DEFINE_AND_INSTANTIATE_GPU_UNARY_FUNC(sign, y[index] = (Dtype(0) < x[index])
                                      - (x[index] < Dtype(0)));
DEFINE_AND_INSTANTIATE_GPU_UNARY_FUNC(sgnbit, y[index] = signbit(x[index]));
DEFINE_AND_INSTANTIATE_GPU_UNARY_FUNC(fabs, y[index] = fabs(x[index]));

__global__ void popc_kernel(const int n, const float* a,
    const float* b, uint8_t* y) {
  CUDA_KERNEL_LOOP(index, n) {
    y[index] = __popc(static_cast<uint32_t>(a[index]) ^
                      static_cast<uint32_t>(b[index]));
  }
}

__global__ void popcll_kernel(const int n, const double* a,
    const double* b, uint8_t* y) {
  CUDA_KERNEL_LOOP(index, n) {
    y[index] = __popcll(static_cast<uint64_t>(a[index]) ^
                      static_cast<uint64_t>(b[index]));
  }
}

template <>
uint32_t caffe_gpu_hamming_distance<float>(const int n, const float* x,
                                  const float* y) {
  // TODO: Fix caffe_gpu_hamming_distance (see failing unit test
  // TestHammingDistanceGPU in test_math_functions.cpp).
  NOT_IMPLEMENTED;
  thrust::device_vector<uint8_t> popcounts(n);
  // NOLINT_NEXT_LINE(whitespace/operators)
  popc_kernel<<<CAFFE_GET_BLOCKS(n), CAFFE_CUDA_NUM_THREADS>>>(
      n, x, y, thrust::raw_pointer_cast(popcounts.data()));
  return thrust::reduce(popcounts.begin(), popcounts.end(),
                        (uint32_t) 0, thrust::plus<uint32_t>());
}

template <>
uint32_t caffe_gpu_hamming_distance<double>(const int n, const double* x,
                                   const double* y) {
  // TODO: Fix caffe_gpu_hamming_distance (see failing unit test
  // TestHammingDistanceGPU in test_math_functions.cpp).
  NOT_IMPLEMENTED;
  thrust::device_vector<uint8_t> popcounts(n);
  // NOLINT_NEXT_LINE(whitespace/operators)
  popcll_kernel<<<CAFFE_GET_BLOCKS(n), CAFFE_CUDA_NUM_THREADS>>>(
      n, x, y, thrust::raw_pointer_cast(popcounts.data()));
  return thrust::reduce(popcounts.begin(), popcounts.end(),
                        /* NOLINT_NEXT_LINE(build/include_what_you_use) */
                        (uint32_t) 0, thrust::plus<uint32_t>());
}

void caffe_gpu_rng_uniform(const int n, unsigned int* r) {
  CURAND_CHECK(curandGenerate(Caffe::curand_generator(), r, n));
}

template <>
void caffe_gpu_rng_uniform<float>(const int n, const float a, const float b,
                                  float* r) {
  CURAND_CHECK(curandGenerateUniform(Caffe::curand_generator(), r, n));
  const float range = b - a;
  if (range != static_cast<float>(1)) {
    caffe_gpu_scal(n, range, r);
  }
  if (a != static_cast<float>(0)) {
    caffe_gpu_add_scalar(n, a, r);
  }
}

template <>
void caffe_gpu_rng_uniform<double>(const int n, const double a, const double b,
                                   double* r) {
  CURAND_CHECK(curandGenerateUniformDouble(Caffe::curand_generator(), r, n));
  const double range = b - a;
  if (range != static_cast<double>(1)) {
    caffe_gpu_scal(n, range, r);
  }
  if (a != static_cast<double>(0)) {
    caffe_gpu_add_scalar(n, a, r);
  }
}

template <>
void caffe_gpu_rng_gaussian(const int n, const float mu, const float sigma,
                            float* r) {
  CURAND_CHECK(
      curandGenerateNormal(Caffe::curand_generator(), r, n, mu, sigma));
}

template <>
void caffe_gpu_rng_gaussian(const int n, const double mu, const double sigma,
                            double* r) {
  CURAND_CHECK(
      curandGenerateNormalDouble(Caffe::curand_generator(), r, n, mu, sigma));
}

}  // namespace caffe

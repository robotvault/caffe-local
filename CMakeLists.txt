# -DCMAKE_BUILD_TYPE=Debug
CMAKE_MINIMUM_REQUIRED(VERSION 2.6)
project( caffe )

SET(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${PROJECT_SOURCE_DIR}/CMakeModules/")



# Overide with cmake -DCMAKE_BUILD_TYPE=Debug {dir}
IF( NOT CMAKE_BUILD_TYPE )
   SET( CMAKE_BUILD_TYPE Release )
ENDIF()

# Platform configuration vars
#INCLUDE(SetPlatformVars)



FIND_PACKAGE(CUDA REQUIRED)
#FIND_PACKAGE(OpenCV REQUIRED)
#FIND_PACKAGE(GLEW REQUIRED)
#FIND_PACKAGE(VTK REQUIRED)
#FIND_PACKAGE(Pangolin REQUIRED)
#FIND_PACKAGE(freenect REQUIRED)
#FIND_PACKAGE(freenectsync REQUIRED)
#FIND_PACKAGE(Cg REQUIRED)
#FIND_PACKAGE(MinimalImgUtilities REQUIRED)
#FIND_PACKAGE(OpenMP REQUIRED)
#FIND_PACKAGE(Icarus REQUIRED)
#FIND_PACKAGE(G2O REQUIRED)
#FIND_PACKAGE(ASSIMP REQUIRED)
#FIND_PACKAGE(Sophus REQUIRED)
#FIND_PACKAGE(Eigen3 REQUIRED)
#FIND_PACKAGE(CSparse)
#FIND_PACKAGE(OpenNI2 REQUIRED)
FIND_PACKAGE(CVD REQUIRED)
# http://gcc.gnu.org/wiki/openmp
FIND_PACKAGE(OpenMP REQUIRED)

#FIND_PACKAGE(PCL REQUIRED)

#FIND_PATH(CUDA_CUT_INCLUDE_DIR
#   cutil.h
#   PATHS ${CUDA_SDK_SEARCH_PATH}
#   PATH_SUFFIXES "common/inc"
#   DOC "Location of cutil.h"
#   NO_DEFAULT_PATH
#)


# Define helper functions and macros used by Google Test.
include(cmake/internal_utils.cmake)
config_compiler_and_linker()  # Defined in internal_utils.cmake.

#INCLUDE(${PROJECT_SOURCE_DIR}/CMakeModules/Find_SSE.cmake)



#http://stackoverflow.com/questions/7081820/setting-default-compiler-in-cmake
#CMAKE_CC_COMPILER=gcc-4.6
#CMAKE_CXX_COMPILER=g++-4.6

SET(
  SOURCES

  ./src/caffe/blob.cpp
  ./src/caffe/common.cpp
  ./src/caffe/layer_factory.cpp
  ./src/caffe/net.cpp
  ./src/caffe/solver.cpp
  ./src/caffe/syncedmem.cpp



  ./src/caffe/layers/accuracy_layer.cpp
  ./src/caffe/layers/data_layer.cu
  ./src/caffe/layers/hdf5_data_layer.cpp
  ./src/caffe/layers/inner_product_layer.cpp 
  ./src/caffe/layers/neuron_layer.cpp 
  ./src/caffe/layers/sigmoid_layer.cpp 
  ./src/caffe/layers/threshold_layer.cpp

  ./src/caffe/layers/argmax_layer.cpp
  ./src/caffe/layers/dropout_layer.cpp
  ./src/caffe/layers/hdf5_data_layer.cu  
  ./src/caffe/layers/inner_product_layer.cu 
  ./src/caffe/layers/pooling_layer.cpp  
  ./src/caffe/layers/sigmoid_layer.cu  
  ./src/caffe/layers/threshold_layer.cu

  ./src/caffe/layers/bnll_layer.cpp
  ./src/caffe/layers/dropout_layer.cu
  ./src/caffe/layers/hdf5_output_layer.cpp 
  ./src/caffe/layers/loss_layer.cpp 
  ./src/caffe/layers/softmax_layer.cpp  
  ./src/caffe/layers/window_data_layer.cpp

  ./src/caffe/layers/bnll_layer.cu
  ./src/caffe/layers/dummy_data_layer.cpp
  ./src/caffe/layers/hdf5_output_layer.cu
  ./src/caffe/layers/lrn_layer.cpp
  ./src/caffe/layers/pooling_layer.cu 
  ./src/caffe/layers/softmax_layer.cu 
  ./src/caffe/layers/window_data_layer.cu

  ./src/caffe/layers/concat_layer.cpp 
  ./src/caffe/layers/eltwise_layer.cpp   
  ./src/caffe/layers/hinge_loss_layer.cpp  
  ./src/caffe/layers/lrn_layer.cu
  ./src/caffe/layers/power_layer.cpp  
  ./src/caffe/layers/softmax_loss_layer.cpp

  ./src/caffe/layers/concat_layer.cu 
  ./src/caffe/layers/eltwise_layer.cu 
  ./src/caffe/layers/im2col_layer.cpp  
  ./src/caffe/layers/memory_data_layer.cpp
  ./src/caffe/layers/power_layer.cu
  ./src/caffe/layers/softmax_loss_layer.cu

  ./src/caffe/layers/conv_layer.cpp           
  ./src/caffe/layers/euclidean_loss_layer.cpp			
  ./src/caffe/layers/im2col_layer.cu
  ./src/caffe/layers/multi_label_accuracy_layer.cpp 
  ./src/caffe/layers/relu_layer.cpp  
  ./src/caffe/layers/split_layer.cpp

  ./src/caffe/layers/euclidean_loss_layer.cu
  ./src/caffe/layers/image_data_layer.cpp 
  ./src/caffe/layers/multi_label_loss_layer.cpp  
  ./src/caffe/layers/relu_layer.cu  
  ./src/caffe/layers/split_layer.cu
  ./src/caffe/layers/conv_layer.cu  

  ./src/caffe/layers/flatten_layer.cpp   
  ./src/caffe/layers/image_data_layer.cu  
  ./src/caffe/layers/multi_label_loss_layer.cu  
  ./src/caffe/layers/sigmoid_cross_entropy_loss_layer.cpp
  ./src/caffe/layers/tanh_layer.cpp

  ./src/caffe/layers/data_layer.cpp 
  ./src/caffe/layers/data_layer_dense.cpp 
  ./src/caffe/layers/flatten_layer.cu  
  ./src/caffe/layers/infogain_loss_layer.cpp 
  ./src/caffe/layers/multinomial_logistic_loss_layer.cpp 
  ./src/caffe/layers/sigmoid_cross_entropy_loss_layer.cu 
  ./src/caffe/layers/tanh_layer.cu


  ./src/caffe/proto/caffe.pb.h
  ./src/caffe/proto/caffe.pb.cc
  ./src/caffe/proto/caffe_pretty_print.pb.h
  ./src/caffe/proto/caffe_pretty_print.pb.cc


  ./src/caffe/util/benchmark.cpp
  ./src/caffe/util/im2col.cpp
  ./src/caffe/util/im2col.cu
  ./src/caffe/util/insert_splits.cpp
  ./src/caffe/util/io.cpp
  ./src/caffe/util/math_functions.cpp
  ./src/caffe/util/math_functions.cu
  ./src/caffe/util/upgrade_proto.cpp


  ./include/caffe/blob.hpp
  ./include/caffe/caffe.hpp
  ./include/caffe/common.hpp
  ./include/caffe/common_layers.hpp
  ./include/caffe/data_layers.hpp
  ./include/caffe/filler.hpp
  ./include/caffe/layer.hpp
  ./include/caffe/loss_layers.hpp
  ./include/caffe/net.hpp 
  ./include/caffe/neuron_layers.hpp
  ./include/caffe/solver.hpp
  ./include/caffe/syncedmem.hpp
  ./include/caffe/vision_layers.hpp

  ./include/caffe/proto/caffe.pb.h
 

  ./include/caffe/util/benchmark.hpp
  ./include/caffe/util/im2col.hpp
  ./include/caffe/util/insert_splits.hpp
  ./include/caffe/util/io.hpp
  ./include/caffe/util/math_functions.hpp
  ./include/caffe/util/mkl_alternate.hpp
  ./include/caffe/util/rng.hpp
  ./include/caffe/util/upgrade_proto.hpp

)

INCLUDE_DIRECTORIES(
	${CMAKE_CURRENT_SOURCE_DIR}
	${CMAKE_SOURCE_DIR}
	${CMAKE_CURRENT_BINARY_DIR}
        /usr/local/cuda/include/
        /usr/local/cuda/samples/common/inc/

        ${CVD_INCLUDE_DIR}

)

INCLUDE_DIRECTORIES(
    ./src
    ./include
)

LINK_DIRECTORIES(
  /usr/local/cuda/lib64
  ./
)


IF(OPENMP_FOUND)
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${OpenMP_EXE_LINKER_FLAGS}")
ENDIF()

#message("PCL LIBRARY = " ${PCL_LIBRARIES})
#message("PCL INCLUDE LIBRARY = " ${PCL_INCLUDE_DIRS})
#message("VTK INCLUDE DIRS = ${VTK_INCLUDE_DIRS}")

cxx_library(gtest "${cxx_strict}" src/gtest/gtest-all.cpp)
cxx_library(gtest_main "${cxx_strict}" src/gtest/gtest_main.cc)

target_link_libraries(gtest_main gtest)
#target_link_libraries(gtest_main)


LINK_LIBRARIES(
  ${CVD_LIBRARY}
)


LINK_LIBRARIES(
  -lcurand
  -lcudart
  -lcublas
  -lpthread
  -lglog
  -lprotobuf
  -lleveldb
  -lsnappy
  -llmdb
  -lboost_system
  -lhdf5_hl
  -lhdf5
  -lopencv_core
  -lopencv_highgui
  -lopencv_imgproc
  -lcblas
  -lVaFRIC
  -lgtest_main
  -lgtest
)

#MESSAGE(STATUS "Cutil at: ${CUDA_CUT_INCLUDE_DIR}")
#MESSAGE(STATUS "CUDA_SDK at: ${CUDA_SDK_ROOT_DIR}")

SET(CUDA_NVCC_FLAGS  "-arch=sm_30"  "--use_fast_math" "-O3" "-lineinfo")

set (CMAKE_RUNTIME_OUTPUT_DIRECTORY "bin/")


#cxx_executable(sample1_unittest samples gtest_main samples/sample1.cc)
#cxx_executable(test_softmax_dense_layer src/caffe/test gtest_main samples/sample1.cc)


## The compiler option "--use_fast_math" forces the compiler to convert
## function(x) into __function(x) that has higher throughput.

#CUDA_ADD_EXECUTABLE(compute_image_mean
#                    ${SOURCES}
#                    ./tools/compute_image_mean.cpp)

#CUDA_ADD_EXECUTABLE(device_query
#                    ${SOURCES}
#                    ./tools/device_query.cpp)

#CUDA_ADD_EXECUTABLE(finetune_net
#                    ${SOURCES}
#                    ./tools/finetune_net.cpp)


#CUDA_ADD_EXECUTABLE(test_caffe
#                    ${SOURCES}
#                    ./tools/simple_tests_caffe.cpp)


CUDA_ADD_EXECUTABLE(test_softmax_layer_bin
	           ${SOURCES}  
	          ./src/caffe/test/test_softmax_layer.cpp
                  ./src/gtest/gtest_main.cc)

CUDA_ADD_EXECUTABLE(test_inner_product_bin
                   ${SOURCES}
                  ./src/caffe/test/test_inner_product_layer.cpp
                  ./src/gtest/gtest_main.cc)

#CUDA_ADD_EXECUTABLE(test_softmax_dense_layer_bin
#	           ${SOURCES}
#	          ./src/caffe/test/test_softmax_layer.cpp
#                  ./src/gtest/gtest_main.cc)

#CUDA_ADD_EXECUTABLE(test_vafric_label
#                    ./tools/test_vafric_labelling.cpp)

CUDA_ADD_EXECUTABLE(test_net
                    ${SOURCES}
                    ./tools/test_net.cpp)

CUDA_ADD_EXECUTABLE(train_net
                    ${SOURCES}
                    ./tools/train_net.cpp)

#CUDA_ADD_EXECUTABLE(main_getVaFRIC_colours
#                        ./tools/main_VaFRIC_colours.cpp)

#CUDA_ADD_EXECUTABLE(convertdata_leveldb
#                    ${SOURCES}
#                    ./tools/convert_vafric_leveldb.cpp)

#CUDA_ADD_EXECUTABLE(upgrade_net_proto_text
#                    ${SOURCES}
#                    ./tools/upgrade_net_proto_text.cpp)

#CUDA_ADD_EXECUTABLE(convert_imageset
#                    ${SOURCES}
#                    ./tools/convert_imageset.cpp)

#CUDA_ADD_EXECUTABLE(dump_networks
#                    ${SOURCES}
#                    ./tools/dump_network.cpp)

#CUDA_ADD_EXECUTABLE(extract_features
#                    ${SOURCES}
#                    ./tools/extract_features.cpp)

#CUDA_ADD_EXECUTABLE(net_speed_benchmark
#                    ${SOURCES}
#                    ./tools/net_speed_benchmark.cpp)

#CUDA_ADD_EXECUTABLE(update_net_proto_binary
#                    ${SOURCES}
#                    ./tools/upgrade_net_proto_binary.cpp)

#CUDA_ADD_EXECUTABLE(test_convert
#  		    ${SOURCES}
#                    ./examples/cifar10/convert_cifar_data.cpp)

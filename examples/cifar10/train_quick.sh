#!/usr/bin/env sh

#TOOLS=../../build/tools
TOOLS=../../build/bin

GLOG_logtostderr=1 $TOOLS/train_net cifar10_quick_solver.prototxt
#GLOG_logtostderr=1 $TOOLS/train_net.bin cifar10_quick_solver.prototxt

#reduce learning rate by fctor of 10 after 8 epochs
GLOG_logtostderr=1 $TOOLS/train_net cifar10_quick_solver_lr1.prototxt cifar10_quick_iter_4000.solverstate
#GLOG_logtostderr=1 $TOOLS/train_net.bin cifar10_quick_solver_lr1.prototxt cifar10_quick_iter_4000.solverstate
